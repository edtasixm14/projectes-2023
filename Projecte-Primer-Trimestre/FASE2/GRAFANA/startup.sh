#!/bin/bash
# Copiem els fitxers de configuració del servidor.
cp /opt/docker/grafana/grafana.ini /etc/grafana/grafana.ini
cp /opt/docker/grafana/ldap.toml /etc/grafana/ldap.toml

#  dashboards i datasource per defecte. 
mkdir /var/lib/grafana
cp -r /opt/docker/grafana/dashboards /var/lib/grafana/
cp -r /opt/docker/grafana/provisioning /usr/share/grafana/conf

# Encenem serveis. 
./influxdb/usr/bin/influxd &
cd telegraf/usr/bin
./telegraf --config telegraf.conf &
cd /usr/share/grafana/bin/
./grafana-server --config /etc/grafana/grafana.ini

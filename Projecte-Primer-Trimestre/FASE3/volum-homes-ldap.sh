# !/bin/bash
# --------------------------------------------------------------------
usuaris_ldap=$(ldapsearch -x -LLL uid | grep uid: | cut -d" " -f2)
for usuari in $usuaris_ldap
		do
		home="/home/ldap/$usuari"
			if  [ -d  $home ] 
			then
				rm -r $home
			else
				mkdir -p $home
				chown -R $usuari $home
			fi
		done

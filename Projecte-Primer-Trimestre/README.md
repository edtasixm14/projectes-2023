
>>>>>>>> # PROJECTE TRIMESTRAL

## INTEGRANTS:

> *· Maria Marlene Flor Benitez.*
>
> *· Kevin Gabriel Gonzalez Trujillo.*
>
> *· Bruno Rodríguez Aranibar.*
>
> *· Lucas Rodríguez Cabañeros.*

## ENLLAÇOS

· __GITLAB__

> <https://gitlab.com/a190074lr/projecte-trimestral>

· __DOCKER HUB__

> <https://hub.docker.com/repository/docker/marleneflor/projecte1t>

## REQUERIMENTS GENERALS

> *· Cal fer grups de dos alumnes per fer aquesta pràctica.*
>
> *· Cal disposar de 4 ordinadors a l’aula, 2 que són les estacions de treball habituals dels alumnes i dos mini-debian.*
>
> *· La pràctica es desenvolupa com un projecte al gitlab entre els dos alumnes.*
>
> *· Cal fer tota la documentació en markdown.*
>
> *· Generar una presentació fent-la en markdown i convertir-la amb pandoc a presentació.*
>
> *· Fer una exposició del projecte amb un temps màxim de 20 minuts.*

## 1. FASE 1: AUTENTICACIÓ LDAP AMB HOMES TMPFS

> Aquesta fase consisteix en implementar en els hosts locals de l’aula (mini debian) l’autenticació LDAP i el servei de homes amb tmpfs i nfs. Els serveis LDAP i NFS es despleguen al cloud.

> ### 1.1 HOSTS CLIENTS

>> En els hosts clients (els hosts de l’aula) s’ha de configura l’autenticació dels usuaris permetent usuaris unix i usuaris LDAP. A tots els usuaris se’ls ha de crear dins del seu home un recurs tmpfs de 100MB. Podeu consultar les intruccions d’instal·lació d’inici de curs a: <https://gitlab.com/manelmellado/ubnt-at-inf/>.

>>> #### 1.1.1 UBICACIÓ ARXIUS

>>>> *· /projecte-trimestral/FASE1/PAM*

>>> #### 1.1.2 EDICIÓ ARXIUS

>>>>· __nslcd.conf__

>>>>> ```
>>>>> # The location at which the LDAP server(s) should be reachable.
>>>>> uri ldap://ldap.edt.org
>>>>>
>>>>> # The search base that will be used for all queries.
>>>>> base dc=edt,dc=org
>>>>> ```

>>>>· __nsswitch.conf__

>>>>> Mirar els recursos de LDAP.

>>>>> ```
>>>>> passwd:         files ldap
>>>>> group:          files ldap
>>>>> ```

>>>>· __ldap.conf__

>>>>> Introduir quina base de dades ldap utilitzarà.

>>>>> ```
>>>>> BASE dc=edt,dc=org
>>>>> URI ldap://ldap.edt.org
>>>>> ```

>>>>· __commom-session__

>>>>> Afegir l'opció de què creï un directori home, a l'hora d'iniciar sessió i faci el muntatges dels homes.

>>>>> ```
>>>>> session	required	pam_unix.so
>>>>> session   optional	pam_mkhomedir.so
>>>>> session	optional	pam_mount.so
>>>>> session	optional	pam_ldap.so
>>>>>```

>>>>· __pam_mount.conf.xml__

>>>>> Configurem el fitxer __pam_mount.conf.xml__ per tal que a tots els usuaris se’ls hi creï dins del seu home un recurs tmpfs de 100MB.

>>>>> ```
>>>>> <!-- Volume definitions -->
>>>>>		<volume 
>>>>>       fstype="tmpfs" 
>>>>>		mountpoint="/home/%(USER)/tmp"  
>>>>>		options="size=100M,uid=%(USER),mode=0700" />
>>>>> ```

>>> #### 1.1.3 CREACIÓ DEL SCRIPT STARTUP

>>>> Creem el script __startup__, l'encarregat de copiar-ne els fitxers ja modificats al container per tal que quan inici el servei ho faci amb les opcions i configuracions que hem construït abans.

>>>> ```
>>>> #! /bin/bash
>>>>
>>>> for user in unix01 unix02 unix03 unix04 unix05
>>>> do
>>>> 	useradd -m -s /bin/bash $user
>>>>	echo -e "$user\n$user" | passwd $user
>>>> done
>>>>
>>>> cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
>>>> cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
>>>> cp /opt/docker/nslcd.conf /etc/nslcd.conf
>>>> cp /opt/docker/common-session /etc/pam.d/common-session
>>>>
>>>> /usr/sbin/nscd
>>>> /usr/sbin/nslcd
>>>> /bin/bash
>>>> ```

>>> #### 1.1.4 CREACIÓ DEL DOCKERFILE

>>>> ```
>>>> # ldapserver 2022
>>>>
>>>> FROM debian:latest
>>>> LABEL version="1.0"
>>>> LABEL author="@edt ASIX-M06"
>>>> LABEL subject="PAM host"
>>>> RUN apt-get update
>>>> #ARG DEBIAN_FRONTEND=noninteractive
>>>> RUN apt-get install -y procps iproute2 iputils-ping nmap tree vim finger passwd libpam-pwquality libpam-mount libpam-ldapd libnss-ldapd ldap-utils nslcd nslcd-utils
>>>> RUN mkdir /opt/docker/
>>>> COPY * /opt/docker/
>>>> RUN chmod +x /opt/docker/startup.sh
>>>> WORKDIR /opt/docker/
>>>> CMD /opt/docker/startup.sh
>>>> ```

>>> #### 1.1.5 INICIALITZACIÓ CONTAINER

>>>> ##### 1.1.5.1 CONSTRUIR L'IMATGE

>>>>> ```
>>>>> $ dockerbuild -t marleneflor/projecte1t:pam
>>>>> ```

>>>> ##### 1.1.5.2 ENCENDRE CONTAINER

>>>>> ```
>>>>> $ docker run --rm pam.edt.org -h pam.edt.org --net 2hisx -p 389:389 -d marleneflor/projecte1t:pam
>>>>> ```

> ### 1.2 SERVEI LDAP

>> Desplegar usant un fitxer compose.yml el servei LDAP en una màquina del Cloud. El servei LDAP ha de disposar de dos volums un de dades i un de configuració. També ha de ser programable segons l’argument rebut a l’entrypoint. Ha de permetre inicialitzar de nou la base de dades o simplement engegar el servidor usant la base de dades i les dades existents.

>>>· __initdb:__ *Crea de nou tota la base de dades LDAP.*
>>>
>>>· __start:__ *Inicialitza el servei LDAP usant la configuració i les dades ja existents en els volums.*

>>> #### 1.2.1 UBICACIÓ ARXIUS

>>>> *· /projecte-trimestral/FASE1/LDAP*

>>> #### 1.2.1 ARXIUS

>>>> *Haurem de disposar dels arxius __edt-org.ldif__ i del arxiu __slapd.conf__, configurats i omplerts amb les dates corresponents d'anteriors pràctiques.*

>>> #### 1.2.3 CREACIÓ DEL SCRIPT STARTUP

>>>> Creem el script __startup__, l'encarregat de inicialitzar el servei LDAP amb les característiques corresponents, segons el'argument rebut a l'entrypoint.

>>>> ```
>>>> # Incialitzar la BD ldap segons l'argument introduït i les seves característiques corresponents.
>>>>
>>>> FILE=/var/lib/ldap/.bdcreat
>>>>
>>>> if [ -f "$FILE" ]; then
>>>> 	/usr/sbin/slapd -d0
>>>>
>>>> else
>>>> 	echo "Inicialització BD ldap edt.org"
>>>> 	rm -rf /etc/ldap/slapd.d/*
>>>>	rm -rf /var/lib/ldap/*
>>>>	slaptest -f slapd.conf -F /etc/ldap/slapd.d
>>>>	slapadd -F /etc/ldap/slapd.d/ -l edt-org.ldif
>>>>	chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap
>>>>	touch /var/lib/ldap/.bdcreat
>>>>	/usr/sbin/slapd -d0
>>>> fi
>>>> exit 0
>>>> ```

>>> #### 1.2.4 CREACIÓ DEL DOCKERFILE

>>>> ```
>>>> # ldapserver 2022
>>>>
>>>> FROM debian:latest
>>>> LABEL author="@edt ASIX Curs 2022"
>>>> LABEL subject="ldapserver 2022"
>>>> ARG DEBIAN_FRONTEND=noninteractive
>>>> RUN apt-get update
>>>> RUN apt-get install -y procps iproute2 iputils-ping nmap tree slapd ldap-utils
>>>> RUN mkdir /opt/docker/
>>>> WORKDIR /opt/docker/
>>>> COPY * /opt/docker/
>>>> RUN chmod +x /opt/docker/startup.sh
>>>> ENTRYPOINT [ "/bin/bash", "/opt/docker/startup.sh" ]
>>>> EXPOSE 389
>>>> ```

>>> #### 1.2.5 INICIALITZACIÓ CONTAINER

>>>> ##### 1.2.5.1 CONSTRUIR L'IMATGE

>>>>> ```
>>>>> $ dockerbuild -t marleneflor/projecte1t:ldap
>>>>> ```

>>>> ##### 1.2.5.2 ENCENDRE CONTAINER

>>>>> ```
>>>>> $ docker run --rm ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d marleneflor/projecte1t:ldap
>>>>> ```

> ### 1.3 SERVEI PHPLDAPADMIN

>> Desplegar al cloud usant el mateix fitxer compose.yml un servidor phpldapadmin que permet accedir a l’administració del servidor LDAP.

>>> #### 1.3.1 UBICACIÓ ARXIUS

>>>> *· /projecte-trimestral/FASE1/ADMIN*

>>> #### 1.3.2 CREACIÓ DEL SCRIPT STARTUP

>>>> Creem el script __startup__, l'encarregat de inicialitzar el servei PHPLDAPADMIN.

>>>> ```
>>>> #! /bin/bash
>>>> bash /opt/docker/install.sh
>>>> /sbin/php-fpm
>>>> /usr/sbin/httpd -D FOREGROUND
>>>> #httpd -S
>>>> #echo $(pgrep httpd)
>>>> ```

>>> #### 1.3.3 CREACIÓ DEL DOCKERFILE

>>>> ```
>>>> # phpldapadmin
>>>> FROM fedora:27
>>>> LABEL version="1.0"
>>>> LABEL author="@edt ASIX-M06"
>>>> LABEL subject="phpldapadmin"
>>>> RUN dnf -y install phpldapadmin php-xml httpd
>>>> RUN mkdir /opt/docker
>>>> COPY * /opt/docker/
>>>> RUN chmod +x /opt/docker/startup.sh
>>>> WORKDIR /opt/docker
>>>> CMD /opt/docker/startup.sh
>>>> EXPOSE 80
>>>> ```

>>> #### 1.3.4 INICIALITZACIÓ CONTAINER

>>>> ##### 1.3.4.1 CONSTRUIR L'IMATGE

>>>>> ```
>>>>> $ dockerbuild -t marleneflor/projecte1t:phpldapadmin
>>>>> ```

>>>> ##### 1.3.4.2 ENCENDRE CONTAINER

>>>>> ```
>>>>> $ docker run --rm phpldapadmin.edt.org -h phpldapadmin.edt.org --net 2hisx -p 389:389 -d marleneflor/projecte1t:phpldapadmin
>>>>> ```

## 2. FASE 2: SERVEI DNS I MONITORITZACIÓ AMB GRAFANA

> Aquesta segona fase consisteix en afegir un servei de DNS que permet identificar tots els hosta de l’aula (tots, no només els 4 del projecte) en el domini edt.org. també inclou un servidor de monitorització amb Grafana per observar el rendiment tant del host amfitrió dels serveis com dels propis serveis.

> ### 2.1 SERVEI DNS

>> Implementar al cloud usant el mateix compose.yml un servidor DNS que permeti identificar tots els hosts de l’aula pel seu nom en el domini edt.org

>>> #### 2.1.1 UBICACIÓ ARXIUS

>>>> *· /projecte-trimestral/FASE2/DNS*

>>> #### 2.1.2 CREACIÓ D'ARXIUS

>>>>· __named.conf__

>>>>> ```
>>>>> options {	
>>>>>     listen-on port 53 { any; };
>>>>>     allow-query     { any; };
>>>>>
>>>>> logging {
>>>>>        channel default_debug {
>>>>>                file "data/named.run";
>>>>>                severity dynamic;
>>>>>        };
>>>>> };
>>>>>
>>>>> zone "edt.org" IN {
>>>>> type master;
>>>>> file "edt.org.zone";
>>>>> };
>>>>>
>>>>> zone "245.200.10.in-addr.arpa" IN {
>>>>> type master;
>>>>> file "reverse.edt.org.zone";
>>>>> };
>>>>>
>>>>> zone "." IN {
>>>>> 	      type hint;
>>>>>	      file "named.ca";
>>>>> };
>>>>>
>>>>> include "/etc/named.rfc1912.zones";
>>>>> include "/etc/named.root.key";
>>>>> ```

>>>>· __edt.org.zone__

>>>>> ```
>>>>> @ IN SOA dns admin.edt.org. ( 1 10800 3600 604800 38400 )
>>>>> @ IN NS dns
>>>>> dns A 172.18.0.2
>>>>> g01 A 10.200.245.201
>>>>> g02 A 10.200.245.202
>>>>> g03 A 10.200.245.203
>>>>> g04 A 10.200.245.204
>>>>> g05 A 10.200.245.205
>>>>> g06 A 10.200.245.206
>>>>> g07 A 10.200.245.207
>>>>> g08 A 10.200.245.208
>>>>> g09 A 10.200.245.209
>>>>> g10 A 10.200.245.210
>>>>> g11 A 10.200.245.211
>>>>> g12 A 10.200.245.212
>>>>> g13 A 10.200.245.213
>>>>> g14 A 10.200.245.214
>>>>> g15 A 10.200.245.215
>>>>> g16 A 10.200.245.216
>>>>> g17 A 10.200.245.217
>>>>> g18 A 10.200.245.218
>>>>> g19 A 10.200.245.219
>>>>> g20 A 10.200.245.220
>>>>> g21 A 10.200.245.221
>>>>> g22 A 10.200.245.222
>>>>> g23 A 10.200.245.223
>>>>> g24 A 10.200.245.224
>>>>> g25 A 10.200.245.225
>>>>> g26 A 10.200.245.226
>>>>> g27 A 10.200.245.227
>>>>> g28 A 10.200.245.228
>>>>> g29 A 10.200.245.229
>>>>> g30 A 10.200.245.230
>>>>> g31 A 10.200.245.231
>>>>> g32 A 10.200.245.232
>>>>> g33 A 10.200.245.233
>>>>> g34 A 10.200.245.234
>>>>> ```

>>>>· __reverse.edt.org.zone__

>>>>> ```
>>>>> @ IN SOA dns admin.edt.org. ( 1 10800 3600 604800 38400 )
>>>>> @ IN NS dns.edt.org.
>>>>> 2 IN PTR dns.edt.org.
>>>>> 201     PTR         g01.edt.org
>>>>> 202     PTR         g02.edt.org
>>>>> 203     PTR         g03.edt.org
>>>>> 204     PTR         g04.edt.org
>>>>> 205     PTR         g05.edt.org
>>>>> 206     PTR         g06.edt.org
>>>>> 207     PTR         g07.edt.org
>>>>> 208     PTR         g08.edt.org
>>>>> 209     PTR         g09.edt.org
>>>>> 210     PTR         g10.edt.org
>>>>> 211     PTR         g11.edt.org
>>>>> 212     PTR         g12.edt.org
>>>>> 213     PTR         g13.edt.org
>>>>> 214     PTR         g14.edt.org
>>>>> 215     PTR         g15.edt.org
>>>>> 216     PTR         g16.edt.org
>>>>> 217     PTR         g17.edt.org
>>>>> 218     PTR         g18.edt.org
>>>>> 219     PTR         g19.edt.org
>>>>> 220     PTR         g20.edt.org
>>>>> 221     PTR         g21.edt.org
>>>>> 222     PTR         g22.edt.org
>>>>> 223     PTR         g23.edt.org
>>>>> 224     PTR         g24.edt.org
>>>>> 225     PTR         g25.edt.org
>>>>> 226     PTR         g26.edt.org
>>>>> 227     PTR         g27.edt.org
>>>>> 228     PTR         g28.edt.org
>>>>> 229     PTR         g29.edt.org
>>>>> 230     PTR         g30.edt.org
>>>>> 231     PTR         g31.edt.org
>>>>> 232     PTR         g32.edt.org
>>>>> 233     PTR         g33.edt.org
>>>>> 234     PTR         g34.edt.org
>>>>> ```

>>> #### 2.1.3 CREACIÓ DEL SCRIPT STARTUP

>>>> Creem el script __startup__, l'encarregat de inicialitzar el servei PHPLDAPADMIN.

>>>> ```
>>>> cp /opt/docker/edt.org.zone /var/named/edt.org.zone
>>>> cp /opt/docker/named.conf /etc/named.conf
>>>> cp /opt/docker/reverse.edt.org.zone /var/named/reverse.edt.org.zone
>>>> chgrp named -R /var/named
>>>> chown -v root:named /etc/named.conf
>>>> restorecon -rv /var/named
>>>> restorecon /etc/named.conf
>>>> /usr/sbin/named -u named -f
>>>> ```

>>> #### 2.1.4 CREACIÓ DEL DOCKERFILE

>>>> ```
>>>> FROM centos:7
>>>> LABEL author="@gabrielggt66"
>>>> LABEL subject="dnsserver 2022"
>>>> RUN  yum install bind bind-utils nmap -y
>>>> RUN mkdir /opt/docker
>>>> WORKDIR /opt/docker
>>>> COPY * /opt/docker/
>>>> RUN chmod +x /opt/docker/startup.sh
>>>> CMD /opt/docker/startup.sh
>>>> EXPOSE 53/udp 53/tcp
>>>> ```

>>> #### 2.1.4 INICIALITZACIÓ CONTAINER

>>>> ##### 2.1.4.1 CONSTRUIR L'IMATGE

>>>>> ```
>>>>> $ dockerbuild -t marleneflor/projecte1t:dns
>>>>> ```

>>>> ##### 2.1.4.2 ENCENDRE CONTAINER

>>>>> ```
>>>>> $ docker run --rm --name dns.edt.org -h dns.edt.org --net 2hisx -p 53:53/udp -p 53:53 --privileged -d marleneflor/projecte1t:dns
>>>>> ```

> ### 2.2 SERVEI GRAFANA

>> Implementar en un container al clous usant el compose.yml un servei de monitorització Grafana. Aquest servei ha de poder ser accessible per els usuaris de LDAP i també per usuaris autenticats a través de proveïdors externs (Third party auth providers) com per exemple gmail.

>> El servei monitoritza el rendiment del sistema operatiu del host amfitrió del cloud on s’han desplegat els containers i també monitoritza cada un dels serveis individualment.

>>> #### 2.2.1 UBICACIÓ ARXIUS

>>>> *· /projecte-trimestral/FASE2/GRAFANA*

>>> #### 2.2.2 CREACIÓ DOCKERFILE

>>>> El contenidor grafana es munta amb una imatge Debian, on totes les ordres les executem amb l'usuari root.

>>>> També obrim els ports 3000,8086,8088
>>>> ```
>>>> FROM grafana/grafana
>>>> LABEL author="@projectit"
>>>> LABEL subject="grafanaserver 2022"
>>>> USER root
>>>> WORKDIR /opt/docker
>>>> COPY . /opt/docker/
>>>> RUN chmod +x /opt/docker/startup.sh
>>>> ENTRYPOINT ["/bin/bash","/opt/docker/startup.sh"]
>>>> ```

>>> #### 2.2.3 CREACIÓ DEL SCRIPT STARTUP

>>>> És copien els fitxers de configuració de ldap als seus respectius llocs.

>>>> ```
>>>> #!/bin/bash
>>>> # Copiem els fitxers de configuració del servidor.
>>>> cp /opt/docker/grafana/grafana.ini /etc/grafana/grafana.ini
>>>> cp /opt/docker/grafana/ldap.toml /etc/grafana/ldap.toml
>>>>
>>>> #  dashboards i datasource per defecte.
>>>> mkdir /var/lib/grafana
>>>> cp -r /opt/docker/grafana/dashboards /var/lib/grafana/
>>>> cp -r /opt/docker/grafana/provisioning /usr/share/grafana/conf
>>>>
>>>> # Encenem serveis.
>>>> ./influxdb/usr/bin/influxd &
>>>> cd telegraf/usr/bin
>>>> ./telegraf --config telegraf.conf &
>>>> cd /usr/share/grafana/bin/
>>>> ./grafana-server --config /etc/grafana/grafana.ini
>>>> ```

>>> #### 2.2.4 CREACIÓ D'ARXIUS DE CONFIGURACIÓ

>>>>· __grafana/defaults.ini(grafana.ini)__

>>>>> *Dins del defaults.ini modifiquem el paràmetre __enabled__, que permet l'autenticació ldap ( per defecte false ).*

>>>>> ```
>>>>> [auth.ldap]
>>>>> enabled = true
>>>>> ```

>>>>· __grafana/ldap.toml__

>>>>> *Especifiquem el contenidor ldap i el port 389.*

>>>>> ```
>>>>> [[servers]]
>>>>> # Ldap server host (specify multiple hosts space separated)
>>>>> host = "ldap.edt.org"
>>>>> # Default port is 389 or 636 if use_ssl = true
>>>>> port = 389
>>>>> ```

>>>>> *Definim amb quin usuari de ldap farem les consultes en la base de dades. En aquest cas amb __Manager__ i el sue password per poder consultar a tos els usuaris.*

>>>>> ```
>>>>> # Search user bind dn
>>>>> bind_dn = "cn=Manager,dc=edt,dc=org"
>>>>> # Search user bind password
>>>>> # If the password contains # or ; you have to wrap it with triple quotes.
>>>>> bind_password = 'secret'
>>>>> ```

>>>>> *Posem com s'ha de buscar e identificar als usuaris de ldap. COm en l'última versió de ldap estan definits que s'identifiquin mitjançant __uid__ posem aquesta opció.*

>>>>> ```
>>>>> # User search filter, for example "(cn=%s)" or "(sAMAccountName=%s)" or "(uid=%s)"
>>>>> search_filter = "(uid=%s)"
>>>>> ```

>>>>> *Especifiquem la base de dades a la que és conectarà.*

>>>>> ```
>>>>> # An array of base dns to search through
>>>>> search_base_dns = ["dc=edt,dc=org"]
>>>>> ```

>>> #### 2.2.5 GOOGLE ( THIRD PARTY AUTH PROVIDERS )

>>>> Per habilitar __Google OAuth2__ en grafana, haurem d'enregistrar la nostra aplicació amb Google. Google generarà un __ID de client__ i __una clau secreta__ pel seu ús.

>>>>> ##### 2.2.5.1 CONFIGURACIÓ EN GOOGLE CLOUD

>>>>>> 1. Anem a <https://console.developers.google.com/apis/credentials>.

>>>>>> 2. Fem click a __Crear credenciales__, després fem click en __ID de client de OAuth__ en el menú desplegable.

>>>>>> 3. Introduïm el següent:

>>>>>>>· __Tipo de aplicación:__ *Aplicación web*

>>>>>>>· __Nombre:__ *Grafana*

>>>>>>>· __Orígenes autorizados de JavaScript:__ *<https://grafana.mycompany.com>*

>>>>>>>· __URL de redirección autorizadas:__ *<https://grafana.mycompany.com/login/google>*

>>>>>>>· *Reemplacem <https://grafana.mycompany.com> amb la URL de la nostra instancia de Grafana.*

>>>>>> 4. Fem click en __Crear__

>>>>>> 5. Copiem el ID del client i el secrel del cliente del model __Cliente OAuth__.

>>>>>> 6. Editem el fitxer __grafana.ini__.

>>>>>>>> ```
>>>>>>>> [auth.google]
>>>>>>>> enabled = true
>>>>>>>> allow_sign_up = true
>>>>>>>> client_id = 734414321237-nkdkp9i7b0id8rstmh180plsucue162t.apps.googleusercontent.com
>>>>>>>> client_secret = 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
>>>>>>>> scopes = https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/>>>>>>>> userinfo.email
>>>>>>>> auth_url = https://accounts.google.com/o/oauth2/auth
>>>>>>>> token_url = https://accounts.google.com/o/oauth2/token
>>>>>>>> api_url = https://www.googleapis.com/oauth2/v1/userinfo
>>>>>>>> allowed_domains =
>>>>>>>> hosted_domain =
>>>>>>>> ```

>>> #### 2.2.6 GITLAB ( THIRD PARTY AUTH PROVIDERS )

>>>> Els passos de configuració en __Gitlab__ són similars als de __Google__.

>>>>> ##### 2.2.6.1 CONFIGURACIÓ EN GITLAB

>>>>>> Editem el fitxer __grafana.ini__.

>>>>>>>> ```
>>>>>>>> #################################### GitLab Auth #########################
>>>>>>>> [auth.gitlab]
>>>>>>>> enabled = true
>>>>>>>> allow_sign_up = true
>>>>>>>> client_id = da2c6a8438e3d293be46959f070b1483923782efe6fde5eff32df8032f7e4094
>>>>>>>> client_secret = 185cdb4e846ab186f53f3cec15b7d6d79cb67508c6a9d1d8f124cf86f9e602cc
>>>>>>>> scopes = api
>>>>>>>> auth_url = https://gitlab.com/oauth/authorize
>>>>>>>> token_url = https://gitlab.com/oauth/token
>>>>>>>> api_url = https://gitlab.com/api/v4
>>>>>>>> allowed_domains =
>>>>>>>> allowed_groups =
>>>>>>>> role_attribute_path =
>>>>>>>> role_attribute_strict = false
>>>>>>>> allow_assign_grafana_admin = false
>>>>>>>> ```

>>> #### 2.2.7 CONFIGURACIÓ GRAFANA

>>>> __NOTA:__ Totes aquestes accions s'automatitzaran des del Dockerfile i startup.sh per incorporar-les al compose final.

>>>> ```
>>>> $ docker run --rm -d --name=grafana --user=root -p 3000:3000 -p 8086:8086 -p 8088:8088 --net=2hisx marleneflor/projecte1t:grafana
>>>> ```

>>>> Esquema lògic infraestructura: <https://www.jorgedelacruz.es/2020/11/23/en-busca-del-dashboard-perfecto-influxdb-telegraf-y-grafana-parte-i-instalando-influxdb-telegraf-y-grafana-sobre-ubuntu-20-04-lts/>

>>>> ![Infraestructura](https://www.jorgedelacruz.es/wp-content/uploads/2017/06/tig-monitor-logic.png)

>>>> ##### 2.2.7.1 INSTAL·LACIÓ I CONFIGURACIÓ DE INFLUXDB I TELEGRAF

>>>>> __Influxdb__

>>>>> ```
>>>>> $ wget https://dl.influxdata.com/influxdb/releases/influxdb-1.8.10_linux_amd64.tar.gz tar xvfz influxdb-1.8.10_linux_amd64.tar.gz
>>>>> ```

>>>>> __Telegraft__

>>>>> ```
>>>>> $ wget https://dl.influxdata.com/telegraf/releases/telegraf-1.25.0_linux_amd64.tar.gz tar xf telegraf-1.25.0_linux_amd64.tar.gz
>>>>> ```

>>>>> __Quedant-se les carpetes:__

>>>>>>· *influxdb-1.8.10-1*

>>>>>>· *telegraf-1.25.0*

>>>>> Per comoditat les anomenem com __influxdb__ i __telegraf__.

>>>>> Fitxer de config telegraf amb plugins necessaris per a la monitorització del sistema i del servei __opendldab__.

>>>>> Creem un fitxer de configuració de config base.

>>>>> ```
>>>>> cd telegraf
>>>>> ./telegraf -sample-config -input-filter >>>>> cpu:kernel:mem:swap:disk:diskio:system:net:processes:openldap -output-filter influxdb > >>>>> telegraf.conf
>>>>> ```

>>>>> Editem el fitxer de cofig de __telegraf.conf__ per recopilar mètriques del servidor OpenLDAP especificant l'adreça IP o el nom de host i el número de port correcte .

>>>>> ```
>>>>> vi telegraf.conf
>>>>> # OpenLDAP cn=Monitor >>>>> plugin                                                                             
>>>>> [[inputs.>>>>>openldap]]                                                                               
>>>>> host = "ldap.edt.org"         # cambiar pro hotsname del container de ldap                                                              
>>>>>  port = 389     
>>>>>
>>>>>  # dn/password to bind with. If bind_dn is empty, an anonymous bind is performed. 
>>>>>  bind_dn = "cn=Manager,dc=edt,dc=org"                                                                   
>>>>>  bind_password = "secret"
>>>>> ```

>>>>> Llancem els executables __influxdb__ i __telegraf__ en __background.__

>>>>> ```
>>>>> workdir /opt/docker
>>>>> ./influxdb/usr/bin/influxd & 
>>>>> ./telegraf --config telegraf.conf & 
>>>>> ```

>>>>> Validacions en la BBDD de influxdb (opcional) desde el dir __influxdb/usr/bin/__.

>>>>> ```
>>>>> ./influx 
>>>>> Connected to http://localhost:8086 version 1.8.10
>>>>> InfluxDB shell version: 1.8.10
>>>>> > 
>>>>> show databases
>>>>> use telegraf
>>>>> show MEASUREMENTS
>>>>> ```

>>> #### 2.2.8 CONNEXIÓ GRAFANA

>>>> Connectar-se per la web: <http://localhost:3000/>

>>>>>· user admin
>>>>>· passwd admin

>>>> ##### 2.2.8.1 CONFIGURATION Y DASHBOARDS

>>>>> Definim la nostra __Data Source (InfluxDB)__

>>>>> ![Data Source](https://gitlab.com/a190074lr/projecte-trimestral/-/blob/main/FILES/IMAGES/Source.JPG)

>>>>> __URL:__ *<http://localhost:8086> , la ip del host i sent el port 8086 per ón escolta influxdb.*

>>>>> ![Influxdb](https://gitlab.com/a190074lr/projecte-trimestral/-/blob/main/FILES/IMAGES/influxdb.JPG)

>>>>> La BBDD és __telegraf__, creada per defecte sense user ni password. 

>>>>> Per la creació dels __dashboard__ importarem els fitxers __.json__ disponibles en la web de grafana.

>>>>> ![Dashboard](https://gitlab.com/a190074lr/projecte-trimestral/-/blob/main/FILES/IMAGES/dashboard.JPG)

>>>>> ![Dashboard2](https://gitlab.com/a190074lr/projecte-trimestral/-/blob/main/FILES/IMAGES/dashboard2.JPG)

>>>>> Finalment visualitzem les métricas obtingudas del sistema del host i del servi __openldap__.

>>>>> ![Métricas](https://gitlab.com/a190074lr/projecte-trimestral/-/blob/main/FILES/IMAGES/metricas.JPG)


## 3. FASE 3: MUNTATGE DE HOMES DELS USUARIS AMB NFS

> En aquesta fase cal que els usuaris LDAP disposin d’un home d’usuari muntat des
d’un servidor NFS que contindrà en un volum els homes dels usuaris

> ### 3.1 SERVEI NFS

>> Desplegar al cloud usant el mateix fitxer compose.yml un servidor NFS que conté en
un volum els directoris home dels usuaris LDAP. Aquest container ha de permetre
autenticació LDAP. S’ha de poder configurar segons el valor d’argument passat a
l’entry point:

>>>· __createHomes:__ *Elimina si existeixen tots els homes i els crea de nou. Es tracta d’un script que ha de generar per a cada usuari LDAP el seu directori home en un recurs compartit el NFS.*

>>>· __syncHomes:__ *Sincronitza de nou els homes dels usuaris afegint aquells homes que no existeixen (usuaris nous al LDAP que no tenen encara un home creat al volum de homes).*

>>>· __start:__ *Inicia el servei NFS exportant els homes dels usuaris (funcionament normal) suposant que el volum de dades ja conté els homes apropiats.*

>>> #### 3.1.1 UBICACIÓ ARXIUS

>>>> *· /projecte-trimestral/FASE3*

>>> #### 3.1.2 ARXIUS

>>>> *Haurem de disposar dels arxius __exports__, __lapd.conf__, __nslcd.conf__, __nsswitch.conf__, __pam_mount.conf__, configurats i omplerts amb les dates corresponents d'anteriors pràctiques.*

>>> #### 3.1.3 CREACIÓ DEL SCRIPT STARTUP

>>>> Creem el script __startup__, l'encarregat de inicialitzar el servei LDAP amb les característiques corresponents, segons el'argument rebut a l'entrypoint.

>>>> ```
>>>> #! /bin/bash
>>>>
>>>> # crear usuaris unix
>>>> for user in unix01 unix02 unix03 unix04 unix05
>>>> do
>>>>   useradd -m -s /bin/bash $user
>>>>   echo -e "$user\n$user" | passwd $user
>>>> done
>>>>
>>>> # engegar serveis per fer el pam_ldap
>>>> /usr/sbin/nslcd
>>>> /usr/sbin/nscd
>>>>
>>>> # deixar el container interactiu
>>>> /bin/bash
>>>>
>>>> ```

>>> #### 3.1.3 CREACIÓ DEL DOCKERFILE

>>>> ```
>>>> # PAM Server 2022>>>>
>>>> FROM debian
>>>> LABEL author="@projectit"
>>>> LABEL subject="NFS 2022"
>>>> # variable para hacer la instalacion desatendida, no interactiva
>>>> ARG DEBIAN_FRONTEND=noninteractive
>>>> RUN apt-get update && apt-get -y install procps tree nmap vim iproute2 libpam-mount libnss-ldapd libpam-ldapd nfs-kernel-server
>>>> RUN mkdir /opt/docker
>>>> WORKDIR /opt/docker
>>>> COPY * /opt/docker/
>>>> COPY ldap.conf /etc/ldap/
>>>> COPY nslcd.conf /etc/
>>>> COPY common-session /etc/pam.d/
>>>> COPY pam_mount.conf.xml /etc/security/
>>>> COPY nsswitch.conf /etc/
>>>> RUN chmod +x /opt/docker/startup.sh
>>>> CMD /opt/docker/startup.sh
>>>> ```

>>> #### 3.1.4 INICIALITZACIÓ CONTAINER

>>>> ##### 3.1.4.1 CONSTRUIR L'IMATGE

>>>>> ```
>>>>> $ dockerbuild -t marleneflor/projecte1t:nfs
>>>>> ```

>>>> ##### 3.1.4.2 ENCENDRE CONTAINER

>>>>> ```
>>>>> $ docker run --rm --name pam.edt.org -h pam.edt.prg --net 2hisx --privileged -it marleneflor/projecte1t:nfs
>>>>> ```

## 4. FASE 4: MUNTATGE DE HOMES DELS USUARIS AMB SSHFS

> En aquesta fase cal que els usuaris LDAP disposin d’un home d’usuari muntat des
d’un servidor __SSHFS__ que contindrà en un volum els homes dels usuaris.

> ### 4.1 SERVEI SSHFS

>> Desplegar al cloud usant el mateix fitxer compose.yml un servidor SSH que conté en
un volum els directoris home dels usuaris LDAP. Aquest container ha de permetre
autenticació LDAP. S’ha de poder configurar segons el valor d’argument passat a
l’entry point:

>>>· __createHomes:__ *Elimina si existeixen tots els homes i els crea de nou. Es tracta d’un script que ha de generar per a cada usuari LDAP el seu directori home als que s’accedeix via SSHFS.*

>>>· __syncHomes:__ *Sincronitza de nou els homes dels usuaris afegint aquells homes que no existeixen (usuaris nous al LDAP que no tenen encara un home creat al volum de homes).*

>>>· __start:__ *Inicia el servei SSH permetent l’accés remot als homes dels usuaris (funcionament normal) suposant que el volum de dades ja conté els homes apropiats.*

>>> #### 4.1.1 UBICACIÓ ARXIUS

>>>> *· /projecte-trimestral/FASE4*

>>> #### 4.1.2 SSHFS_CLIENT

>>>> ##### 4.1.2.1 ARXIUS

>>>>> *Haurem de disposar dels arxius __common-account__, __common-auth__, __common_password__, __common-session__, __ldap.conf__, __login.defs__, __nscld.conf__, __nsswitch.conf__ configurats i omplerts amb les dates corresponents d'anteriors pràctiques.*

>>>> ##### 4.1.2.2 EDICIÓ PAM_MOUNT.CONF

>>>>> ```
>>>>> <volume fstype="fuse"
>>>>>       user="*"
>>>>>       uid="5000-10000"
>>>>>       path="sshfs#%(USER)@sshfs.edt.org:./."
>>>>>       mountpoint="~/%(USER)"
>>>>>       options="port=22,nosuid,nodev,noatime,reconnect,allow_other,default_permissions password_stdin"
>>>>>       ssh="0" noroot="0" 
>>>>>	    />
>>>>> ```

>>>> ##### 4.1.2.3 CREACIÓ DEL SCRIPT STARTUP

>>>>> Creem el script __startup__, l'encarregat de inicialitzar el servei LDAP amb les característiques corresponents, segons el'argument rebut a l'entrypoint.

>>>>> ```
>>>>> #! /bin/bash
>>>>> for user in unix01 unix02 unix03 unix04 unix05
>>>>> do
>>>>>  useradd -m -s /bin/bash $user
>>>>>  echo -e "$user\n$user" | passwd $user
>>>>> done
>>>>>
>>>>> cp /opt/docker/nslcd.conf /etc/nslcd.conf
>>>>> cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
>>>>> cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
>>>>> #cp /opt/docker/common-account /etc/pam.d/common-account
>>>>> #cp /opt/docker/common-auth /etc/pam.d/common-auth
>>>>> #cp /opt/docker/common-password /etc/pam.d/common-password
>>>>> cp /opt/docker/common-session /etc/pam.d/common-session
>>>>> cp /opt/docker/pam_mount.conf.xml /etc/security/pam_mount.conf.xml
>>>>>
>>>>> /usr/sbin/nscd
>>>>> /usr/sbin/nslcd
>>>>>
>>>>> bash /opt/docker/ldapusers.sh
>>>>> mkdir /root/.ssh
>>>>> chmod 700 /root/.ssh
>>>>> #cp /opt/docker/known_hosts /root/.ssh/known_hosts
>>>>> ssh-keyscan sshfs.edt.org >> /root/.ssh/known_hosts
>>>>>
>>>>> mkdir /run/sshd
>>>>> /usr/sbin/sshd  -D
>>>>> ```

>>>> ##### 4.1.2.4 CREACIÓ DEL DOCKERFILE

>>>>> ```
>>>>> # ldapserver
>>>>> FROM debian:latest
>>>>> LABEL author="@projectit"
>>>>> LABEL subject="SSHFS CLIENT 2022"
>>>>> RUN apt-get update
>>>>> #ARG DEBIAN_FRONTEND=noninteractive
>>>>> RUN apt-get -y install procps iproute2 tree nmap vim less finger passwd libpam-pwquality libpam-mount libnss-ldapd libpam-ldapd nslcd nslcd-utils ldap-utils  openssh-client openssh-server sshfs
>>>>> RUN mkdir /opt/docker
>>>>> COPY * /opt/docker/
>>>>> RUN chmod +x /opt/docker/startup.sh
>>>>> WORKDIR /opt/docker
>>>>> CMD /opt/docker/startup.sh
>>>>> ```

>>> #### 4.1.2 SSHFS_SERVIDOR

>>>> ##### 4.1.2.1 ARXIUS

>>>>> *Haurem de disposar dels arxius __common-account__, __common-auth__, __common_password__, __common-session__, __ldap.conf__, __login.defs__, __nscld.conf__, __nsswitch.conf__, __pam_mount.conf__ configurats i omplerts amb les dates corresponents d'anteriors pràctiques.*

>>>> ##### 4.1.2.2 CREACIÓ DEL SCRIPT STARTUP

>>>>> Creem el script __startup__, l'encarregat de inicialitzar el servei LDAP amb les característiques corresponents, segons el'argument rebut a l'entrypoint.

>>>>> ```
>>>>> #! /bin/bash
>>>>> for user in unix01 unix02 unix03 unix04 unix05
>>>>> do
>>>>>  useradd -m -s /bin/bash $user
>>>>>  echo -e "$user\n$user" | passwd $user
>>>>> done
>>>>>
>>>>> cp /opt/docker/nslcd.conf /etc/nslcd.conf
>>>>> cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
>>>>> cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
>>>>> #cp /opt/docker/common-account /etc/pam.d/common-account
>>>>> #cp /opt/docker/common-auth /etc/pam.d/common-auth
>>>>> #cp /opt/docker/common-password /etc/pam.d/common-password
>>>>> cp /opt/docker/common-session /etc/pam.d/common-session
>>>>> cp /opt/docker/pam_mount.conf.xml /etc/security/pam_mount.conf.xml
>>>>>
>>>>> /usr/sbin/nscd
>>>>> /usr/sbin/nslcd
>>>>> 
>>>>> usuaris_ldap=$(ldapsearch -x -LLL uid | grep uid: | cut -d" " -f2)
>>>>> case $1 in
>>>>>	"createHomes")
>>>>>		for usuari in $usuaris_ldap
>>>>>		do
>>>>>		home=$(getent passwd | grep "^$usuari:" | cut -d: -f6)
>>>>>			if  [ -d  $home ] 
>>>>>			then
>>>>>				rm -r $home
>>>>>			else
>>>>>				mkdir -p $home
>>>>>				chown -R $usuari $home
>>>>>			fi
>>>>>		done
>>>>>		mkdir /run/sshd
>>>>>		/usr/sbin/sshd  -D;;
>>>>>	"syncHomes")
>>>>>		for usuari in $usuaris_ldap
>>>>>		do
>>>>>		home=$(getent passwd | grep "^$usuari:" | cut -d: -f6)
>>>>>		if  ![ -d  $home ]
>>>>>		then
>>>>>			mkdir -p $home
>>>>>			chown -R $usuari $home
>>>>>		fi
>>>>>		done
>>>>>		mkdir /run/sshd
>>>>>		/usr/sbin/sshd  -D;;
>>>>>	"start")
>>>>>		mkdir /run/sshd
>>>>>		/usr/sbin/sshd  -D;;
>>>>> esac
>>>>> ```

>>>> ##### 4.1.2.2 CREACIÓ DEL DOCKERFILE

>>>>> ```
>>>>> # ldapserver
>>>>> FROM debian:latest
>>>>> LABEL author="@projectit"
>>>>> LABEL subject="SSHFS SERVIDOR 2022"
>>>>> RUN apt-get update
>>>>> #ARG DEBIAN_FRONTEND=noninteractive
>>>>> RUN apt-get -y install procps iproute2 tree nmap vim less finger passwd libpam-pwquality libpam-mount libnss-ldapd libpam-ldapd nslcd nslcd-utils ldap-utils  openssh-client openssh-server
>>>>> RUN mkdir /opt/docker
>>>>> COPY * /opt/docker/
>>>>> RUN chmod +x /opt/docker/startup.sh
>>>>> WORKDIR /opt/docker
>>>>> ENTRYPOINT [ "/bin/bash", "/opt/docker/startup.sh" ]
>>>>> ```

## 5. FASE 5: MUNTATGE DE HOMES DELS USUARIS AMB SSHFS

> En aquesta fase cal que els usuaris LDAP disposin d’un home d’usuari muntat des
d’un servidor __SSHFS__ que contindrà en un volum els homes dels usuaris.

> ### 4.1 SERVEI SSHFS

>>
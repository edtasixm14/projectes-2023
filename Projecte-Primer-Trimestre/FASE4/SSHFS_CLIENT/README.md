>>> #### 4.1.2 SSHFS_CLIENT

>>>> ##### 4.1.2.1 ARXIUS

>>>>> *Haurem de disposar dels arxius __common-account__, __common-auth__, __common_password__, __common-session__, __ldap.conf__, __login.defs__, __nscld.conf__, __nsswitch.conf__ configurats i omplerts amb les dates corresponents d'anteriors pràctiques.*

>>>> ##### 4.1.2.2 EDICIÓ PAM_MOUNT.CONF

>>>>> ```
>>>>> <volume fstype="fuse"
>>>>>       user="*"
>>>>>       uid="5000-10000"
>>>>>       path="sshfs#%(USER)@sshfs.edt.org:./."
>>>>>       mountpoint="~/%(USER)"
>>>>>       options="port=22,nosuid,nodev,noatime,reconnect,allow_other,default_permissions password_stdin"
>>>>>       ssh="0" noroot="0" 
>>>>>	    />
>>>>> ```

>>>> ##### 4.1.2.3 CREACIÓ DEL SCRIPT STARTUP

>>>>> Creem el script __startup__, l'encarregat de inicialitzar el servei LDAP amb les característiques corresponents, segons el'argument rebut a l'entrypoint.

>>>>> ```
>>>>> #! /bin/bash
>>>>> for user in unix01 unix02 unix03 unix04 unix05
>>>>> do
>>>>>  useradd -m -s /bin/bash $user
>>>>>  echo -e "$user\n$user" | passwd $user
>>>>> done
>>>>>
>>>>> cp /opt/docker/nslcd.conf /etc/nslcd.conf
>>>>> cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
>>>>> cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
>>>>> #cp /opt/docker/common-account /etc/pam.d/common-account
>>>>> #cp /opt/docker/common-auth /etc/pam.d/common-auth
>>>>> #cp /opt/docker/common-password /etc/pam.d/common-password
>>>>> cp /opt/docker/common-session /etc/pam.d/common-session
>>>>> cp /opt/docker/pam_mount.conf.xml /etc/security/pam_mount.conf.xml
>>>>>
>>>>> /usr/sbin/nscd
>>>>> /usr/sbin/nslcd
>>>>>
>>>>> bash /opt/docker/ldapusers.sh
>>>>> mkdir /root/.ssh
>>>>> chmod 700 /root/.ssh
>>>>> #cp /opt/docker/known_hosts /root/.ssh/known_hosts
>>>>> ssh-keyscan sshfs.edt.org >> /root/.ssh/known_hosts
>>>>>
>>>>> mkdir /run/sshd
>>>>> /usr/sbin/sshd  -D
>>>>> ```

>>>> ##### 4.1.2.4 CREACIÓ DEL DOCKERFILE

>>>>> ```
>>>>> # ldapserver
>>>>> FROM debian:latest
>>>>> LABEL author="@projectit"
>>>>> LABEL subject="SSHFS CLIENT 2022"
>>>>> RUN apt-get update
>>>>> #ARG DEBIAN_FRONTEND=noninteractive
>>>>> RUN apt-get -y install procps iproute2 tree nmap vim less finger passwd libpam-pwquality libpam-mount libnss-ldapd libpam-ldapd nslcd nslcd-utils ldap-utils  openssh-client openssh-server sshfs
>>>>> RUN mkdir /opt/docker
>>>>> COPY * /opt/docker/
>>>>> RUN chmod +x /opt/docker/startup.sh
>>>>> WORKDIR /opt/docker
>>>>> CMD /opt/docker/startup.sh
>>>>> ```
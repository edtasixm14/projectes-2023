> ### 1.1 HOSTS CLIENTS

>> En els hosts clients (els hosts de l’aula) s’ha de configura l’autenticació dels usuaris permetent usuaris unix i usuaris LDAP. A tots els usuaris se’ls ha de crear dins del seu home un recurs tmpfs de 100MB. Podeu consultar les intruccions d’instal·lació d’inici de curs a: <https://gitlab.com/manelmellado/ubnt-at-inf/>.

>>> #### 1.1.1 UBICACIÓ ARXIUS

>>>> *· /projecte-trimestral/FASE1/PAM*

>>> #### 1.1.2 EDICIÓ ARXIUS

>>>>· __nslcd.conf__

>>>>> ```
>>>>> # The location at which the LDAP server(s) should be reachable.
>>>>> uri ldap://ldap.edt.org
>>>>>
>>>>> # The search base that will be used for all queries.
>>>>> base dc=edt,dc=org
>>>>> ```

>>>>· __nsswitch.conf__

>>>>> Mirar els recursos de LDAP.

>>>>> ```
>>>>> passwd:         files ldap
>>>>> group:          files ldap
>>>>> ```

>>>>· __ldap.conf__

>>>>> Introduir quina base de dades ldap utilitzarà.

>>>>> ```
>>>>> BASE dc=edt,dc=org
>>>>> URI ldap://ldap.edt.org
>>>>> ```

>>>>· __commom-session__

>>>>> Afegir l'opció de què creï un directori home, a l'hora d'iniciar sessió i faci el muntatges dels homes.

>>>>> ```
>>>>> session	required	pam_unix.so
>>>>> session   optional	pam_mkhomedir.so
>>>>> session	optional	pam_mount.so
>>>>> session	optional	pam_ldap.so
>>>>>```

>>>>· __pam_mount.conf.xml__

>>>>> Configurem el fitxer __pam_mount.conf.xml__ per tal que a tots els usuaris se’ls hi creï dins del seu home un recurs tmpfs de 100MB.

>>>>> ```
>>>>> <!-- Volume definitions -->
>>>>>		<volume 
>>>>>       fstype="tmpfs" 
>>>>>		mountpoint="/home/%(USER)/tmp"  
>>>>>		options="size=100M,uid=%(USER),mode=0700" />
>>>>> ```

>>> #### 1.1.3 CREACIÓ DEL SCRIPT STARTUP

>>>> Creem el script __startup__, l'encarregat de copiar-ne els fitxers ja modificats al container per tal que quan inici el servei ho faci amb les opcions i configuracions que hem construït abans.

>>>> ```
>>>> #! /bin/bash
>>>>
>>>> for user in unix01 unix02 unix03 unix04 unix05
>>>> do
>>>> 	useradd -m -s /bin/bash $user
>>>>	echo -e "$user\n$user" | passwd $user
>>>> done
>>>>
>>>> cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
>>>> cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
>>>> cp /opt/docker/nslcd.conf /etc/nslcd.conf
>>>> cp /opt/docker/common-session /etc/pam.d/common-session
>>>>
>>>> /usr/sbin/nscd
>>>> /usr/sbin/nslcd
>>>> /bin/bash
>>>> ```

>>> #### 1.1.4 CREACIÓ DEL DOCKERFILE

>>>> ```
>>>> # ldapserver 2022
>>>>
>>>> FROM debian:latest
>>>> LABEL version="1.0"
>>>> LABEL author="@edt ASIX-M06"
>>>> LABEL subject="PAM host"
>>>> RUN apt-get update
>>>> #ARG DEBIAN_FRONTEND=noninteractive
>>>> RUN apt-get install -y procps iproute2 iputils-ping nmap tree vim finger passwd libpam-pwquality libpam-mount libpam-ldapd libnss-ldapd ldap-utils nslcd nslcd-utils
>>>> RUN mkdir /opt/docker/
>>>> COPY * /opt/docker/
>>>> RUN chmod +x /opt/docker/startup.sh
>>>> WORKDIR /opt/docker/
>>>> CMD /opt/docker/startup.sh
>>>> ```

>>> #### 1.1.5 INICIALITZACIÓ CONTAINER

>>>> ##### 1.1.5.1 CONSTRUIR L'IMATGE

>>>>> ```
>>>>> $ dockerbuild -t marleneflor/projecte1t:pam
>>>>> ```

>>>> ##### 1.1.5.2 ENCENDRE CONTAINER

>>>>> ```
>>>>> $ docker run --rm pam.edt.org -h pam.edt.org --net 2hisx -p 389:389 -d marleneflor/projecte1t:pam
>>>>> ```
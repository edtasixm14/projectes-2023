
> ## Desplegament de instancia AWS amb terraform 

El fitxer **main.tf** crea una instancia en AWS mitjançant terraform que alhora s'aproveix de tot el necesari per a fer funcionar els serveis ldap , psql i serverweb

Les comandes necesaries per a fer funcionar la instancia son:

- Preparar el sistema de terraform amb l'ordre:

```
terraform init
```
- Executar les ordres del main.tf amb la comanda:

```
terraform apply
```

_Nota:Aquestes ordres es tenen que executar al directori on hi ha definit el fitxer **main.tf**_


Per ultim hi ha que modificar el **/etc/hosts** per poder accedir a les pagines web i posar la seguent linia

```
18.133.60.118 web.edt.org webldap.edt.org webapirest.edt.org
```
 
### main.tf
```

terraform {
required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
}
}

provider "aws" {
  region     = "eu-west-2"
  access_key = "AKIAQUYHMRZWXBWMF3NN"
  secret_key = "Josg0ixgQxOuap+JJne/lXN90oVFi3cjLd/ZfKEK"
}

resource "aws_eip_association" "eip_assoc" {
  instance_id   = "${aws_instance.example.id}"
  allocation_id = "eipalloc-0180d32aeba990d61"
}

resource "aws_instance" "example" {
  ami           = "ami-0d93d81bb4899d4cf" // replace with your desired AMI
  instance_type = "t2.small" // replace with your desired instance type
  key_name = "apache"
  vpc_security_group_ids = [aws_security_group.webserver-sg.id]
  tags = {
    Name = "docker-instance"
  }
}


resource "null_resource" "copy_script" {
  depends_on = [aws_instance.example]

  provisioner "file" {
    source      = "./install_docker.sh"
    destination = "/home/admin/install_docker.sh"

    connection {
      type        = "ssh"
      user        = "admin"
      private_key = file("apache.pem")
      host        = "18.133.60.118"
    }
  }
}
resource "null_resource" "copy_yaml" {
  depends_on = [null_resource.copy_script]

  provisioner "file" {
    source      = "./compose.yml"
    destination = "/home/admin/compose.yml"

    connection {
      type        = "ssh"
      user        = "admin"
      private_key = file("apache.pem")
      host        = "18.133.60.118"
    }
  }
}
resource "null_resource" "run_script" {
  depends_on = [null_resource.copy_yaml]

  provisioner "remote-exec" {
    inline = [
      "chmod +x /home/admin/install_docker.sh",
      "bash /home/admin/install_docker.sh",
      "sudo docker compose -f /home/admin/compose.yml up -d"
    ]

    connection {
      type        = "ssh"
      user        = "admin"
      private_key = file("apache.pem")
      host        = "18.133.60.118"
    }
  }
}

resource "aws_security_group" "webserver-sg" {
  name = "webserver-sg"
  description = "Puertos para funcionamiento del servidor web"

  ingress {
    description = "http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "https"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "psql"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "spotify apirest"
    from_port   = 8888
    to_port     = 8888
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "ldap"
    from_port   = 389
    to_port     = 389
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

// Expose the EC2 instance's public IP address
output "public_ip" {
  value = "18.133.60.118"
}

```

> ## Proveidors
Primer hi ha que definir els proveidors que utilitzara terraform i aixi averiguar el proces que volem que faci.

Terraform disposa de una llarga varietat de proveidors pero el que es necessari per a la practica es **aws**.La definicio del parametres que apareixen son:

- __required_providers:__ Dins d'aquest segment definim els proveidors necesaris
- __aws:__ Indiquem com a proveidor **aws** i dins especifiquem mes parametres 
    - __source:__ Es d'on es aquest proveidor i on trobarlo
    - __version:__ Indiquem la versio que ens interesa

```
terraform {
required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
}
}
```
Despres s'indica parametres extras per a una configuracio mes complexa.
En aquest s'indica el seguent:

- __region:__ Es la regio on estar ubicada la nostra instacia
- __access_key i secret_key:__ Son les calus necesaries per asociar la creacio de la intacia a un compte personal de amazon

```
provider "aws" {
  region     = "eu-west-2"
  access_key = "AKIAQUYHMRZWWQQXPQ7P"
  secret_key = "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"
}
```

> ## Asociacio de IP elastica
Per al bon funcionament del servei integrat de spotify fa falta asociar la instancia a una ip elastica de manera que la ip publica sempre sera la mateixa.Aixo es defineix en aquest apartat:

-   __resource "aws_eip_association" "eip_assoc":__ Definim un recurs  que especifica la asociacio de una ip publica a una intancia de AWS **(aws_eip_association)**.La identificacio d'aquest recurs sera **(eip_assoc)**

- __instance_id:__ Es l'identificador de la instancia sobre la cual s'associa la ip elastica.Aquesta s'aconsegueix amb la variable **${aws_instance.example.id}** que s'assigna cuan la instancia de aws es creada.

- __allocation_id:__ Es l'identificador de la ip elastica que utilitzarem i que associarem.
(Per aconseguir la id fa falta crear la ip elastica amb antelacio en la web d'administracio de intancies a Amazon) 

```
resource "aws_eip_association" "eip_assoc" {
  instance_id   = "${aws_instance.example.id}"
  allocation_id = "eipalloc-0180d32aeba990d61"
}
```

> ## Creacio instancia AWS
En aquestpas es crea la instancia de AWS definint caracteristiques importants en la seva creacio com la ami a utilitzar i el tipus d'instancia.Aquests es difineixen d'aquesta manera:

-  __resource "aws_instance" "example":__ Es crea un recurs que crea la intancia aws **(aws_instance)** i te com a identificador **(example)**

- __ami:__ S'especifica la id de la ami que volem utilitzar.En aquest cas
**(ami-0d93d81bb4899d4cf)** seria utilitzar la ami de **(Debian11)**

- __instance_type:__ Diem quina capacitat de procesament volem.Normalment seria una **(t2.micro)** pero com els serveis exigeixen una alta capacitat de processament aqui s'utilitza una **(t2.small)**

- __key_name:__ Es la clau .pem amb la que ens permet conectar a la instancia mitjançant ssh

- __vpc_security_group_ids:__ Aqui es defineix amb que security group s'asociara aquesta instancia i per tant que ports es propagaran.Aquesta esta definida amb la variable **([aws_security_group.webserver-sg.id])**

- __tags:__ Son els tags amb les que s'identificar la intancia

    - __Name:__ Es el nom que tindra la instacia dins de web d'administracio de intancies a Amazon

```
resource "aws_instance" "example" {
  ami           = "ami-0d93d81bb4899d4cf" // replace with your desired AMI
  instance_type = "t2.small" // replace with your desired instance type
  key_name = "apache"
  vpc_security_group_ids = [aws_security_group.webserver-sg.id]
  tags = {
    Name = "docker-instance"
  }
}
```
> ## Copiar fitxers locals a la Instancia

Per a poder fer funcionar els serveis definits en el compose necesitem copiar el recurs **install_docker.sh** i **compose.yml** dins la intancia.El primer arxiu es un script que instala totes les dependencies necesaries per a fer funcionar docker en la instancia i el segon arxiu es un **.yml** que conte els serveis a llançar.

Per a fer la accio esmentada hi ha que definir els seguents parametres dos vegades (un per a copiar install_docker i la'altra per a copiar el compose.yml):

- __resource "null_resource" "copy_script":__ Es un recurs que no pertany a un proveidor en concret **(null_resource)** i te com a identificador **(copy_script)** per a copiar **install_docker** o **(copy_yaml)** per a copiar el **compose.yml**
- __depends_on = [aws_instance.example]:__ Es una condicio d'execucio del recurs el cual no s'executara si el proces definit  ha fallat.En aquest cas seria  **([aws_instance.example])** fa referencia al proces de creacio de la instancia AWS o **([null_resource.copy_script])** que es l'altre recurs per a copiar un fitxer
- __provisioner "file":__ Es el proces per a proveir a la instancia amb un fitxer
  - __source:__ S'indica la ruta del fitxer local que volem copiar
  - __destination:__ S'especifica la ruta desti on es copia el fitxer
- __connection:__ Crea una conexio a la intsancia
  - __type:__ Diem que tipus de conexio volem.En aquest cas **(ssh)**
  - __user:__ Es  l'usuari amb qui ens conectarem.En aquest cas **(admin)**
  - __private_key:__ Definim amb que clau .pem ens verifiquem.En aquest cas **(apache.pem")**
  - __host:__ Comuniquem quina sera la ip del host desti.Aqui hi ha que posar la ip elastica abans creada **(18.133.60.118)**

```
resource "null_resource" "copy_script" {
  depends_on = [aws_instance.example]

  provisioner "file" {
    source      = "./install_docker.sh"
    destination = "/home/admin/install_docker.sh"

    connection {
      type        = "ssh"
      user        = "admin"
      private_key = file("apache.pem")
      host        = "18.133.60.118"
    }
  }
}

resource "null_resource" "copy_yaml" {
  depends_on = [null_resource.copy_script]

  provisioner "file" {
    source      = "./compose.yml"
    destination = "/home/admin/compose.yml"

    connection {
      type        = "ssh"
      user        = "admin"
      private_key = file("apache.pem")
      host        = "18.133.60.118"
    }
  }
}
```

> ## Desplegament dels serveis
Per a desplegar els serveis ho farem de forma que nosaltres proveim les ordres bash per executar el fitxer d'instalacio de docker i posar en marxa el compose.yml amb els serveis.

El recurs necesari es defineix de la seguent forma:

- __resource "null_resource" "run_script":__ Es un recurs que no pertany a un proveidor en concret **(null_resource)** i te com a identificador **(run_script)** 

- __depends_on = [null_resource.copy_yaml]:__ Es una condicio d'execucio del recurs el cual no s'executara si el proces definit  ha fallat.En aquest cas seria  **([null_resource.copy_yaml])**.

- __provisioner "remote-exec":__ Utilitzem un provisioner que executa ordres en la instancia de forma remota **(remote-exec)**.
  - __inline:__ Es proporciona les ordres bash a executar que son els seguents:
    - __chmod +x /home/admin/install_docker.sh__ Cambia els permisos del fitxer **(install_docker.sh)** per a ser un executable
    - __bash /home/admin/install_docker.sh__ Executem **(install_docker.sh)**
    - __sudo docker compose -f /home/admin/compose.yml up -d__ Posem en marxa els serveis en background amb docker compose i el fitxer **(compose.yml)**
- __connection:__ Crea una conexio a la intsancia
  - __type:__ Diem que tipus de conexio volem.En aquest cas **(ssh)**
  - __user:__ Es  l'usuari amb qui ens conectarem.En aquest cas **(admin)**
  - __private_key:__ Definim amb que clau .pem ens verifiquem.En aquest cas **(apache.pem")**
  - __host:__ Comuniquem quina sera la ip del host desti.Aqui hi ha que posar la ip elastica abans creada **(18.133.60.118)**

```
resource "null_resource" "run_script" {
  depends_on = [null_resource.copy_yaml]

  provisioner "remote-exec" {
    inline = [
      "chmod +x /home/admin/install_docker.sh",
      "bash /home/admin/install_docker.sh",
      "sudo docker compose -f /home/admin/compose.yml up -d"
    ]

    connection {
      type        = "ssh"
      user        = "admin"
      private_key = file("apache.pem")
      host        = "18.133.60.118"
    }
  }
}
```
> ## Configurar security groups

  En aquest apartat definim els ports que s'han d'obrir en el security groups vinculat a aquesta instància. Els ports necessaris són: 80 (http), 443 (https), 389 (ldap), 5432 (psql) i el 8888 (aplicació spotify). 

  Els ports a obrir es defineixen de la seguent manera:

  - __resource "aws_security_group" "webserver-sg":__ defineix un recurs per l'especificació de ports a obrir en el security group.
  - __name:__ defineix el nom que tindrà el security group creat.
  - __description:__ opcional per descriure la finalitat del grup de ports.
  - __ingress:__ amb aquesta directiva indiquem el port a obrir.
    - __description:__ descripció del port (opcional)
    - __from_port:__ numero de port inicial que volem obrir
    - __to_port:__ numero de port final 
    - __protocol:__ aqui especifiquem si els ports han de ser tipus TCP o UDP
    - __cidr_blocks:__ especifiquem quin rang d'adreçes o quines adreçes específiques permetem que entrin. En aquest cas utilitzem un wildcard que significa totes les ipv4.
  - __egress:__ en aquesta part es defineix el tràfic de sortida que té les mateixes directives que les d'entrada. Però per defecte AMS permet tot el tràfic de sortida i simplement ho reflectim així amb aquests valors.

  ```
resource "aws_security_group" "webserver-sg" {
  name = "webserver-sg"
  description = "Puertos para funcionamiento del servidor web"

  ingress {
    description = "http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  ingress {
    description = "https"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "psql"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "spotify apirest"
    from_port   = 8888
    to_port     = 8888
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "ldap"
    from_port   = 389
    to_port     = 389
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
  ``` 
> ## Exposar la clau publica

Per ultim pasem per pantalla la ip publica de la instancia per a facilitar una conexio ssh.

Aquest pas es fa d'aquesta forma:
- __output "public_ip":__ S'especifica que es mostrara el valor de una variable anomenada **(public_ip)**.

  - __value = "18.133.60.118":__ El valor d'aquesta variable sera la ip elastica asociada a la instancia.

```
// Expose the EC2 instance's public IP address
output "public_ip" {
  value = "18.133.60.118"
}
```
>>>>>>>>>> # PROJECTE TRIMESTRAL

## INTEGRANTS:

> *· Maria Marlene Flor Benitez.*
>
> *· Kevin Gabriel Gonzalez Trujillo.*
>
> *· Bruno Rodríguez Aranibar.*
>
> *· Lucas Rodríguez Cabañeros.*

## ENLLAÇOS

· __GITLAB__

> <https://gitlab.com/a190074lr/projecte-trimestral/-/tree/main/2%20TRIMESTRE>

· __DOCKER HUB__

> <https://hub.docker.com/u/proyecto2hisx>

## REQUERIMENTS GENERALS

> *· Cal fer grups de dos alumnes per fer aquesta pràctica.*
>
> *· Cal disposar de 4 ordinadors a l’aula, 2 que són les estacions de treball habituals dels alumnes i dos mini-debian.*
>
> *· La pràctica es desenvolupa com un projecte al gitlab entre els dos alumnes.*
>
> *· Cal fer tota la documentació en markdown.*
>
> *· Generar una presentació fent-la en markdown i convertir-la amb pandoc a presentació*
>
> *· Fer una exposició del projecte amb un temps màxim de 20 minuts.*
>
> *· Elaborar un poster del projecte amb el format que es proporcionarà i que ha d’incloure un codi QR que enllaci al Git del projecte.*

## 1. FASE 1: API REST WEB AMB AUTENTICACIÓ OAUTH

> Aquesta fase consisteix en crear una seu web que implementa API-REST i autenticació OAUTH. Per fer-ho es segueix el curs de “Cibernarium” (en principi de 3h...) que construeix un exemple fictici primer i un sobre Spotify després. Tot el procediment a part d’estar detallat en el curs està explicat al Howto següent:
>
>> · Implementar el [HowTo-ASIX-API-Rest](https://gitlab.com/edtasixm14/m14-projectes/-/blob/master/docs/HowTo-ASIX-API-REST.pdf) amb un aplicació web amb autenticació Oauth.
>
>> · Perquè un servidor web es pugui denominar REST o RESTful ha de satisfer sis restriccions, l'última d'elles opcional. Complint amb les restriccions, aconseguim unes propietats desitjables com a rendiment, escalabilitat, variabilitat, portabilitat i fiabilitat:
>
>>> __· Uniform Interface:__ *Defineix una interfície genèrica per a administrar cada interacció que es produeixi entre el client i el servidor de manera uniforme, la qual cosa simplifica i separa l'arquitectura. Aquesta restricció indica que cada recurs del servei REST ha de tenir una única direcció o "URI".*
>
>>> __· Client-Server:__ *Separació entre client i servidor. El client no es preocupa de l'emmagatzematge de les dades i així s'aconsegueix que el seu codi font sigui més portable. Quant al servidor, no es preocupa de l'estat del client, fent que aquest pugui ser més escalable. El desenvolupament del client i del servidor pot ser independent l'u de l'altre, mentre que la interfície uniforme entre els dos no es vegi alterada.*
>
>>> __· Stateless:__ *Aquí declarem que cada petició que rep el servidor hauria de ser independent i contenir tot el necessari per a ser processada.*
>
>>> __· Cacheable:__ *Les respostes del servidor poden guardar-se en una memòria caixet, sigui de manera implícita, explícita o negociada. L'objectiu és minimitzar les interaccions API REST per a plataformes digitals entre client i servidor, fent que el client accedeixi a la representació del recurs guardada en caixet i millorant l'escalabilitat i rendiment del sistema.*
>
>>> __· Layered system:__ *El client no assumeix que hi ha una connexió directa amb el servidor final. Poden existir sistemes de programació o maquinari entre ells. Això ajuda a millorar l'escalabilitat, el rendiment i la seguretat.*
>	
>>> __· Code on Demand (opcional):__ *El servidor, de manera temporal, pot decidir ampliar la funcionalitat del client, transferint-li un codi i executant aquesta lògica.*

> ### 1.1 TIPUS API

>> ![tipos_apis](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/2%20TRIMESTRE/Images/types_apis.png)  

> ### 1.2 MÈTODES HTTP EN UNA API REST

>> Les operacions més importants que ens permetran manipular els recursos i interactuar amb la API són:  
>
>>> ![metodos_http](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/2%20TRIMESTRE/Images/metodos_http.png)  
>
>>> __· GET:__ *S'utilitza per a sol·licitar un recurs específic. Es realitza a través de la URL i la resposta inclou les dades del recurs sol·licitat.*
>
>>> __· POST:__ *S'utilitza per a crear un nou recurs. Les dades s'envien en el cos de la sol·licitud i la resposta inclou una confirmació que s'ha creat el recurs.*
>
>>> __· PUT:__ *Actualitza un recurs existent. Les dades s'envien en el cos de la sol·licitud i la resposta inclou una confirmació que s'ha actualitzat el recurs.*
>
>>> __· DELETE:__ *Elimina un recurs existent. La sol·licitud es realitza a través de la URL i la resposta inclou una confirmació que s'ha eliminat el recurs.*

> ### 1.3 MISSATGES DE RESPOSTA I ERROR 

>> Abans de passar a l'explicació del codi de nostra api, vegem els valors més comuns que sol prendre la propietat estatus en el cas que la petició AJAX es processi satisfactòriament:
>
>>> __· Status 200 =__ *Resposta estàndard quan la petició està OK.*
>
>>> __· Status 201 =__ *En el cas de peticions per POST, petició completada i nou recurs creat.*
>
>> __Errors de la part client:__
>
>>> __· Status 400 =__ *Bad Request -  La petició no es pot completar per un error de sintaxi.*
>
>>> __· Status 401 =__ *Unauthorized -  La petició és legal però el servidor la rebutja.*
>
>>> __· Status 403 =__ *Forbidden -  La petició és legal però el servidor la rebutja.*
>
>>> __· Status 404 =__ *Not found - L'URL indicat no es troba.*
>
>> __Errors per la part del servidor:__
>
>>> __· Status 500 =__ *Internal Server Error - Missatge d'error genèric.*
>
>>> __· Status 503 =__ *Service Unavailable - El servidor no està disponible.*

> ### 1.4 TECNOLOGIA AJAX 

>> __AJAX__(*Asynchronous JavaScript And XML*) és la tecnologia de JavaScript que ens permet implementar anomenades a altres servidors donis de la nostra aplicació. __AJAX ens permet:__
>
>>> *· Llegir dades d'un servidor web (una vegada s'ha carregat la pàgina).*
>
>>> *· Actualitzar una pàgina web sense haver de recarregar la pàgina.*
>
>>> *· Enviar dades a un servidor web.*
>
>> __L'element més important d'AJAX és l'objecte XMLHttpRequest est objecte de JavaScript serveix per a:__
>
>>> *· Fer una petició de dades a un servidor, una vegada s'ha carregat la pàgina.*
>
>>> *· Rebre dades d'un servidor, una vegada s'ha carregat la pàgina.*
>
>>> *· Enviar dades al servidor, en el pla ocult o background de la nostra aplicació.*
>
>>> ![http_esq](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/2%20TRIMESTRE/Images/http_esq.png)
>
>> __EXEMPLE:__
>
>>>> ![request_ajax](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/2%20TRIMESTRE/Images/request_ajax.png)
>
>>>> ![browser_ajax](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/2%20TRIMESTRE/Images/browser_request.png)
>
>> En el desenvolupament de la nostra aplicació utilitzarem jQuery una de les llibreries o frameworks més populars de JavaScript. jQuery facilita la manipulació dels objectes de l'HTML i simplifica les peticions AJAX.
>
>> __Comptant amb diferents mètodes per a realitzar peticions i vincular-los a esdeveniments dels nostres pàgina:__
>
>>> __· $.getJSON():__ *Ens permet fer peticions AJAX sempre que estiguem recollint un JSON del endpoint on fem la consulta.*
>
>>> __· $.get():__ *És equivalent a fer una petició XMLHttpRequest() utilitzant el mètode HTTP GET en JavaScript.*
>
>>> __· $.ajax():__ *Ens permet fer una consulta i manipular també el resultat de l'error, a diferència d'amb els dos mètodes anteriors.*
>
>> Podem accedir a la informació emmagatzemada en una API REST fent una petició específica al endpoint que contingui la informació que vulguem recollir. En molts casos, haurem de sol·licitar credencials perquè el servidor ens autoritzi a accedir a les seves dades.
> 
>> Per a poder treballar amb jQuery, una opció que tenim és instal·lar-nos l'última versió local en el nostre ordinador o una altra manera és fer una anomenada al CDN de Google perquè el carregui directament a la nostra aplicació.
>
>> __Ho farem introduint el següent script dins de index.html en el nostre subdomini webapirest.edt.org:__
>
>>> ```
>>> <script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
>>> ```

> ### 1.5 INTEGRACIÓ

>> Finalment definirem els requeriments de nostra API, prepararem el nostre entorn de treball i escriurem el nostre programa.
>
>>> __· Configuració del compte:__
>
>>>> __Hem de disposar d'un compte de Spotify, ja sigui Premium o gratuïta, amb la qual hem d'entrar en la nostra zona privada fent login en:__ 'https://developer.spotify.com/dashboard'.
>
>>>>> *· Aquí és on podrem autoritzar l'accés de la nostra aplicació a la API de Spotify.*
>
>>> __· Registre de l'aplicació:__
>
>>>> *Podem registrar l'aplicació abans de crear-la. Seguirem els passos indicats de l'apartat “Register your application” de l'anterior enllaç.*
>
>>>> *Generem el CLIENT ANEU que és l'identificador únic de la nostra aplicació.*
>
>>>> ![credentials_spotify](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/2%20TRIMESTRE/Images/credentials_spotify.png)  
>
>>>> *Introduirem el nom i la descripció de l'aplicació i la creem. Entrarem en la vista de la nostra aplicació trobarem el CLIENT ANEU i el CLIENT SECRET, que necessitarem durant la fase d'autenticació. “Client Secret” és la clau que passarem a les crides perquè les peticions siguin més segures.*
>
>>> __· Preparació de l'entorn de treball:__
>
>>>> *Una vegada registrada l'aplicació, instal·larem Node.js per a poder treballar en un entorn d'execució de servidor en JavaScript. __Com root:__*
>
>>>> ```
>>>> https://github.com/nodesource/distributions/blob/master/README.md
>>>> curl -fsSL https://deb.nodesource.com/setup_18.x | bash -
>>>> apt-get install -y nodejs
>>>> ```
>
>>> __· Carpeta de projecte i arxius:__
>
>>>> __Dins de /var/www/webapirest.edt.org es trobaran els següents elements importants:__
>
>>>>>   __· node_modules:__ *Directori que es crea en cada projecte de Node.js per a instal·lar dependències requerides.*
>
>>>>>	__· package.json i package-lock.json:__ *Són arxius que s'usen en els projectes de Node.js per a administrar dependències i paquets.*
>
>>>>>   __· app.js:__ *Codi que executa la nostra aplicació per a autoritzar-nos a fer el login en la nostra app i mostrar una plantilla amb la resposta que recull del endpoint 'https://api.spotify.com/v1/me', corresponent a la informació pública de l'usuari o usuària.*
>
>>>>>   __· spotify.js:__ *Codi que llançarà les peticions cap als endpoints de spotify.*

> ### 1.6 CODI

>> · [app.js](https://gitlab.com/a190074lr/projecte-trimestral/-/blob/main/2%20TRIMESTRE/serverweb/webapirest.edt.org/html/app.js)  
>
>> · [spotify.js](https://gitlab.com/a190074lr/projecte-trimestral/-/blob/main/2%20TRIMESTRE/serverweb/webapirest.edt.org/html/public/scripts/spotify.js)

> ### 1.7 OAUTH 2.0 - OPEN AUTHORIZATION 

>> Estàndard dissenyat per a permetre que les aplicacions de tercers accedeixin als recursos protegits d'un usuari en un servei en línia utilitzant tokens d'accés per a permetre que l'aplicació accedeixi als recursos de l'usuari en nom de l'usuari, sense que l'aplicació hagi de conèixer les credencials de l'usuari.

>> ### 1.7.1 GOOGLE

>>> Per a l'accés al nostre domini https://webapirest.edt.org realitzem la configuració des de Google Cloud Console, on registrem la nostra aplicació anomenada "webapirest" per a obtenir i les claus secretes que hem d'introduir en la configuració del nostre virtualhost per a autoritzar l'accés a l'usuari al nostre subdomini.  
>
>>>>> ![login_gcp](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/2%20TRIMESTRE/Images/login-gcp.png)  
>
>>> Fet el registre, en el nostre servidor hem d'instal·lar el mòdul d'Apatxe __mod_auth_openidc__ que permet realitzar l'autenticació dels usuaris i aplicació el control d'accés basat en els protocols OpenID Connect i OAuth 2.0.
>
>>> ```
>>> sudo yum install mod_auth_openidc
>>> ```
>
>>> __Configurem el virtualhost i reiniciem el servei:__
>
>>>> ```
>>>> <VirtualHost *:443>
>>>>    ... 
>>>>        
>>>>    SSLEngine on
>>>>    SSLCertificateFile /etc/ssl/certs/webapirest.crt
>>>>    SSLCertificateKeyFile /etc/ssl/private/webapirest.key
>>>>
>>>>    OIDCProviderMetadataURL https://accounts.google.com/.well-known/openid-configuration
>>>>    OIDCClientID 734414321237-8cvub0hpjg93734hob6qadqnvb622lao.apps.googleusercontent.com
>>>>    OIDCClientSecret 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
>>>>    OIDCRedirectURI https://webapirest.edt.org/html
>>>>    OIDCCryptoPassphrase jupiter
>>>>    OIDCScope "openid email"
>>>>    
>>>>  <Location / >
>>>>    AuthType openid-connect
>>>>    Require valid-user
>>>>  </Location>
>>>>   ...
>>>> </VirtualHost>
>>>> ```
>
>>>> ```
>>>> sudo systemctl restart apache2
>>>> ```
>
>>> __DIRECTIVAS:__
>
>>>> __· SSLEngine on:__ *Activa l'encriptació SSL.*
>
>>>> __· SSLCertificateFile i SSLCertificateKeyFile:__ *Especifiquen la ubicació de l'arxiu de certificat SSL i la clau privada que utilitzarem per a establir una connexió segura.*
>
>>>> __· OIDCProviderMetadataURL:__ *Especifica la URL del proveïdor d'identitat OIDC. En aquest cas, Google.*
>
>>>> __· OIDCClientID y OIDCClientSecret:__ *Són les credencials de l'aplicació OIDC que permeten que l'aplicació verifiqui la identitat dels usuaris.*
>
>>>> __· OIDCRedirectURI:__ *És l'adreça a la qual es redirigirà l'usuari després que s'autentiqui amb el proveïdor d'identitat OIDC.*
>
>>>> __· OIDCCryptoPassphrase:__ *És una contrasenya utilitzada per a encriptar les claus privades del servidor.*
>
>>>> __· OIDCScope:__ *És una llista separada per espais dels àmbits d'accés sol·licitats per l'aplicació OIDC.*
>
>>>> __· '<Location / >':__ *Especifica que la configuració d'autenticació s'aplica a totes les sol·licituds de l'arrel del lloc web.*
>
>>>> __· AuthType:__ *Especifica el mètode d'autenticació, en aquest cas OIDC.*
>
>>>> __· Require valid-user:__ *Especifica que només es permetran usuaris autenticats.  *

>> ### 1.7.1 SPOTIFY

>>> En relació amb l'autenticació, com ja es va comentar en l'apartat de API REST, totes les peticions a la API de Spotify requereixen una autenticació que es realitza mitjançant l'enviament d'una clau secreta o token en la capçalera de la petició AJAX. En el cas de consultes privades, aquest 'access_token' només es genera una vegada l'usuari autoritza l'accés de l'aplicació a les seves dades. 
>
>>> En relació amb l'autenticació, com ja es va comentar en l'apartat de API REST, totes les peticions a la API de Spotify requereixen una autenticació que es realitza mitjançant l'enviament d'una clau secreta o token en la capçalera de la petició AJAX. En el cas de consultes privades, aquest 'access_token' només es genera una vegada l'usuari autoritza l'accés de l'aplicació a les seves dades.
>
>>>> ![login_apispotify](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/2%20TRIMESTRE/Images/login_apispotify.png)
>
>>> Per a això, aprofitarem la plataforma de Spotify Web Api, que ens permetrà desenvolupar una nova aplicació que utilitzarà recursos de la API de Spotify.
>
>>>> ![user-API-endpoint](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/2%20TRIMESTRE/Images/apirest_endpoint_esq.png)
>
>>> __ESQUEMA:__
>
>>>> *· End User, correspon a l'usuari de Spotify. L'usuari final atorga accés als recursos protegits (per exemple, llistes de reproducció, informació personal, etc.)*
>
>>>> *· My App, és el client que sol·licita accés als recursos protegits (per exemple, una aplicació mòbil o web).*
>
>>>> *· Servidor que allotja els recursos protegits i proporciona autenticació i autorització a través de OAuth 2.0.*

## 2. FASE 2: SERVIDOR WEB AMB SEUS VIRTUALS

> ### 2.1 ESQUEMA DEL PROJECTE

>> En aquest projecte despleguem un servidor web apache amb __3 seus virtuals__ que implementen un tipus d'autenticació diferent en cada una d'elles.

>> La primera __web web.edt.org__ no requereix cap tipus d'autenticació per accedir-ne i és una pàgina estàtica.
>> La segona __webldap.edt.org__ requereix __autenticació LDAP__ i a més és una __web dinàmica__ on es poden fer consultes a un servidor posgresql.
>> La tercera __webapirest.edt.org__ requereix __autencació terceritzada__, en aquest cas ser usuari vàlid de google per accedir. Una vegada accedim a la web, per anar a parar a la app web creada amb la api de spotify ho fem através d'un enllaç que ens porta a la app on una vegada més hem __d'autenticar-nos conforme som usuaris acreditats d'Spotify.__

>> Aquest servidor web treballa en conjunt amb un servidor LDAP, un servidor Postgres i una aplicació web creada utilitzant l'API d'Spotify corrent __en la mateixa màquina que el servidor web.__

>> Utilitzant __terraform__ hem desplegat una màquina a AWS aprovisionada amb docker, amb els ports utilitzats ja oberts dins seu, a més de llençar els tres contenidors de docker corresponents al servidor web, LDAP i postgres amb un fitxer compose automàticament.

>> Aquest és el esquema de conexions amb les màquines:

>> ![Esquema](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/2%20TRIMESTRE/Images/Esquema_proyecto_2do_trimestre.png)

>>> #### 2.1.1 SERVER WEB

>>>> · Web estàtica que serveix simplement de referència per verificar l’accés al servidor.

>>>> __· CREACIÓ CODI HTML__

>>>>> Crear el fitxer __html__ la part que és centra en el funcionament __intern__ i __relacional__ entre les webs.

>>>>> ```
>>>>> <!DOCTYPE html>
>>>>> <html>
>>>>>
>>>>>  <head>
>>>>>    <meta charset="utf-8">
>>>>>    <title>INICI</title>
>>>>>    <link rel="stylesheet" href="style.css">
>>>>>  </head>
>>>>>
>>>>>  <body>
>>>>>
>>>>>    <header>
>>>>>        <h1 class="tittle"> PROJECTE 2N TRIMESTRE </h1>
>>>>>        <center>
>>>>>            <a href="index.html"><img src="images/logo.png" width="30%"></a>
>>>>>        </center>
>>>>>    </header>
>>>>>
>>>>>    <br>
>>>>>
>>>>>    <article>
>>>>>      <br>
>>>>>      <a href="http://webldap.edt.org/index.php" style="font-size: 35px; margin-right: 50px">LDAP</a>
>>>>>      <a href="https://webapirest.edt.org/index.html"style="font-size: 35px;">APIREST</a>
>>>>>      <br><br> 
>>>>>    </article>
>>>>>
>>>>>    <br>
>>>>>
>>>>>    <article>
>>>>>      <u><h1 style="text-align: center;">PRESENTACIÓ</h1></u>
>>>>>      <p>
>>>>>        Click <a href="images/pandoc.pdf" download>aquí</a> aquí per descarregar la presentació feta en <strong>Pandoc.</strong>
>>>>>      </p>
>>>>>      <iframe src="images/pandoc.pdf" width="1050" height="700" name="iframe-a"></iframe>
>>>>>      <br>  <br>
>>>>>    </article>
>>>>>
>>>>>  </body>
>>>>>  
>>>>>  <br>
>>>>>
>>>>>  <footer>
>>>>>    <u>
>>>>>    <br>
>>>>>    <strong> Autors: Maria Marlene Flor Benitez,  Kevin Gabriel Gonzalez Trujillo, Bruno Rodríguez Aranibar i Lucas Rodríguez Cabañeros </strong>
>>>>>    <br>
>>>>>    <strong>Gitlab: <a href="https://gitlab.com/a190074lr/projecte-trimestral/-/tree/main/2%20TRIMESTRE">https://gitlab.com/a190074lr/projecte-trimestral/-/tree/main/2%20TRIMESTRE</strong></a>
>>>>>    <br>
>>>>>    <strong>DockerHub: <a href="https://hub.docker.com/repositories/proyecto2hisx">https://hub.docker.com/repositories/proyecto2hisx</strong></a>
>>>>>    <br>
>>>>>    <strong> All rights deserved: 2HISX SL </strong>
>>>>>    <br><br>
>>>>>    </u>
>>>>>  </footer>
>>>>>
>>>>> </html>
>>>>> ```

>>>>> Crear el fitxer __CSS__ la part que és centra en l'estètica del web.

>>>>> ```
>>>>> body {
>>>>>    background-color: brown;
>>>>> }
>>>>>
>>>>> .tittle{
>>>>>    text-align: center;
>>>>>    padding: 1em;
>>>>>    background-color: rgba(115, 115, 219, 0.923);
>>>>>    border: 5px solid rgb(0,0,0);
>>>>> }
>>>>>
>>>>> .head {
>>>>>    margin-left: 500px;
>>>>> }
>>>>>
>>>>> footer{
>>>>>    background-color: rgba(115, 115, 219, 0.923);
>>>>>    text-align: center;
>>>>>    font-size: medium;
>>>>>    font-style: italic;
>>>>>    border: 4.5px solid rgb(0,0,0);
>>>>> }
>>>>>
>>>>> nav {
>>>>>    background-color: burlywood;
>>>>>    float: inline-start;
>>>>>    padding: 1em;
>>>>>    font-size: 30px;
>>>>>    border: 4.5px solid black;
>>>>>    text-align: center;
>>>>> }
>>>>>
>>>>> article {
>>>>>    text-align: center;
>>>>>    background-color: burlywood;
>>>>>    color: brown;
>>>>>    border: 2.5px solid black;
>>>>>    padding: 3px;
>>>>>    margin-right: 400px;
>>>>>    margin-left: 400px;
>>>>> }
>>>>>
>>>>> .text {
>>>>>    background-color: burlywood;
>>>>>    color: brown;
>>>>>    border: 2.5px solid black;
>>>>>    padding: 3px;
>>>>>    text-align: center;
>>>>> }
>>>>> ```

>>> #### 2.1.2 WEB LDAP

>>>> · Per accedir al contingut d’aquesta web (o d’una de les seves pàgines) caldrà realitzar una autenticació contra el servidor LDAP.
>>
>>>> · Observem també que al accedir al web ens demana autenticació mitjançant usuaris LDAP.
>>
>>>> · Dins d'aquest web podem observar que s'ens ofereix un apartat per realitzar consultes a la nostre BDD de training que està en funcionament en el seu docker corresponent.

>>>> __· CREACIÓ CODI PHP__

>>>>> Crear el fitxer __.php__ en comptes de .html, és la part que és centra en el funcionament __intern__ i __relacional__ entre les webs.

>>>>> ```
>>>>> <!DOCTYPE html>
>>>>> <html>
>>>>>
>>>>> <head>
>>>>>  <meta charset="utf-8">
>>>>>  <title>WEB LDAP</title>
>>>>>  <link rel="stylesheet" href="style.css">
>>>>> </head>
>>>>>
>>>>> <body>
>>>>>
>>>>>  <header>
>>>>>      <h1 class="tittle"> PROJECTE 2N TRIMESTRE </h1>
>>>>>      <center>
>>>>>            <a href="http://web.edt.org/index.html"><img src="images/logo.png" width="30%"></a>
>>>>>      </center>
>>>>>  </header>
>>>>>  <br>
>>>>>
>>>>>  <article>
>>>>>    <br>
>>>>>    <a href="http://webldap.edt.org/index.php" style="font-size: 35px; margin-right: 50px">LDAP</a>
>>>>>    <a href="https://webapirest.edt.org"style="font-size: 35px;">APIREST</a>
>>>>>    <br><br> 
>>>>>  </article>
>>>>>
>>>>>  <br>
>>>>>
>>>>>    <article>
>>>>>      <form method="POST" action="">
>>>>>        <h1><strong><label for="sql">FES LA TEVA CONSULTA SQL</label></strong></h1>
>>>>>		    <textarea id="sql" name="sql" rows="3" cols="125"></textarea>
>>>>>		    <br><br>
>>>>>		    <input type="submit" value="Consultar">
>>>>>        <br><br>
>>>>>	    </form>
>>>>>
>>>>>      <?php
>>>>>      // Conexión a la base de datos training
>>>>>      $dbconn = pg_connect("host=psql.edt.org dbname=training user=prova password=passwd") or die('No se pudo conectar: ' . pg_last_error());
>>>>>      
>>>>>      // si se ingresa un valor en el formulario 
>>>>>      if (isset($_POST["sql"])) {
>>>>>          // Sentencia SQL para extraer los datos que deseamos mostrar
>>>>>          $query = $_POST["sql"];
>>>>>          $result = pg_query($dbconn, $query) or die('La consulta falló: ' . pg_last_error());
>>>>>
>>>>>          // Mostrar los resultados en una tabla HTML
>>>>>          echo "<table>";
>>>>>          echo "<tr>";
>>>>>          // Creamos dinámicamente los encabezados segun el número de campos
>>>>>          for ($i = 0; $i < pg_num_fields($result); $i++) {
>>>>>              echo "<th>" . pg_field_name($result, $i) . "</th>";
>>>>>          }
>>>>>          echo "</tr>";
>>>>>
>>>>>         // pg_fetch_assoc() retorna una matriz asociativa a los datos obtenidos con la sentencia SQL
>>>>>          while ($row = pg_fetch_assoc($result)) {
>>>>>              echo "<tr>";
>>>>>              // Por cada fila, mostramos datos 
>>>>>              foreach ($row as $field) {
>>>>>                  echo "<td>" . $field . "</td>";
>>>>>              }
>>>>>              echo "</tr>";
>>>>>          }
>>>>>          echo "</table>";
>>>>>      }
>>>>>      // Cerrar la conexión a la base de datos
>>>>>      pg_close($dbconn);
>>>>>      ?>
>>>>>    </article>
>>>>>
>>>>>    <br>
>>>>>
>>>>>    <article>
>>>>>      <br>
>>>>>      <strong style="font-size: 15px; margin-right: 10px; list-style-type: none;"><a href="#TAULA CLIENTS">TAULA CLIENTS</a></strong>
>>>>>      <strong style="font-size: 15px; margin-right: 10px"><a href="#TAULA PRODUCTES">TAULA PRODUCTES</a></strong>
>>>>>      <strong style="font-size: 15px; margin-right: 10px"><a href="#TAULA COMANDES">TAULA COMANDES</a></strong>
>>>>>      <strong style="font-size: 15px; margin-right: 10px"><a href="#TAULA OFICINES">TAULA OFICINES</a></strong>
>>>>>      <strong style="font-size: 15px; margin-right: 10px"><a href="#TAULA REPVENTAS">TAULA REPVENTAS</a></strong>
>>>>>      <strong style="font-size: 15px; margin-right: 10px"><a href="#TAULA RELACIONAL">TAULA RELACIONAL</a></strong>
>>>>>      <br><br>
>>>>>    </article>
>>>>>
>>>>>    <br>
>>>>>
>>>>>    <article id="TAULA CLIENTS">
>>>>>      <u><h1>TAULA CLIENTS</h1></u>
>>>>>      <center>
>>>>>        <img src="images/clientes.png">
>>>>>      </center>
>>>>>      <br>
>>>>>    </article>
>>>>>
>>>>>    <br>
>>>>>
>>>>>    <article id="TAULA PRODUCTES">
>>>>>      <u><h1>TAULA PRODUCTES</h1></u>
>>>>>      <center>
>>>>>        <img src="images/productos.png">
>>>>>      </center>
>>>>>      <br>
>>>>>    </article>
>>>>>
>>>>>    <br>
>>>>>
>>>>>    <article id="TAULA COMANDES">
>>>>>      <u><h1>TAULA COMANDES</h1></u>
>>>>>      <center>
>>>>>        <img src="images/pedidos.png">
>>>>>      </center>
>>>>>      <br>
>>>>>    </article>
>>>>>
>>>>>    <br>
>>>>>
>>>>>    <article id="TAULA OFICINES">
>>>>>      <u><h1>TAULA OFICINES</h1></u>
>>>>>      <center>
>>>>>        <img src="images/oficinas.png">
>>>>>      </center>
>>>>>      <br>
>>>>>    </article>
>>>>>
>>>>>    <br>
>>>>>
>>>>>    <article id="TAULA REPVENTAS">
>>>>>      <u><h1>TAULA REPVENTAS</h1></u>
>>>>>      <center>
>>>>>        <img src="images/repventas.png">
>>>>>      </center>
>>>>>      <br>
>>>>>    </article>
>>>>>
>>>>>    <br>
>>>>>
>>>>>    <article id="TAULA RELACIONAL">
>>>>>      <u><h1>TAULA RELACIONAL</h1></u>
>>>>>      <center>
>>>>>        <img src="images/funcionament.png">
>>>>>      </center>
>>>>>      <br>
>>>>>    </article>
>>>>>
>>>>>    <br>
>>>>>
>>>>>  </body>
>>>>>  
>>>>>  <br>
>>>>>
>>>>>  <footer>
>>>>>    <u>
>>>>>    <br>
>>>>>    <strong> Autors: Maria Marlene Flor Benitez,  Kevin Gabriel Gonzalez Trujillo, Bruno Rodríguez Aranibar i Lucas Rodríguez Cabañeros </strong>
>>>>>    <br>
>>>>>    <strong>Gitlab: <a href="https://gitlab.com/a190074lr/projecte-trimestral/-/tree/main/2%20TRIMESTRE">https://gitlab.com/a190074lr/projecte-trimestral/-/tree/main/2%20TRIMESTRE</strong></a>
>>>>>    <br>
>>>>>    <strong>DockerHub: <a href="https://hub.docker.com/repositories/proyecto2hisx">https://hub.docker.com/repositories/proyecto2hisx</strong></a>
>>>>>    <br>
>>>>>    <strong> All rights deserved: 2HISX SL </strong>
>>>>>    <br><br>
>>>>>    </u>
>>>>>  </footer>
>>>>>
>>>>> </html>
>>>>> ```

>>>>> Utilizem el mateix fitxer __CSS__ que hem creat pel __server web__, serà la part que és centra en l'estètica del web.

>>>> __· ATUENTICACIÓ LDAP__

>>>>> Per la web amb autenticació LDAP, vam haver d'afegir algunes lineas per tal de connectar amb la base de dades per poder demanar l'autenticació amb el seus usuaris. Amb les seguents lineas al virtualhost de la web dinàmica amb autenticacio LDAP, estem declarant quin tipus d'autenticació i per part de quin proveidor volem __AuthBasicProvider__, quin és el servidor amb el qual ha de contactar la web per fer l'autenticaicó __AuthLDAPURL__, a nom de qui __AuthLDAPBindDN__,  amb quines credencials __AuthLDAPBindPassword__ i qué és necesari per donar un usuari per vàlid __Require__.

>>>>> ```
>>>>>  AuthType Basic
>>>>>  AuthName "LDAP Authentication"
>>>>>  AuthBasicProvider ldap
>>>>>  AuthLDAPBindDN          "cn=Manager,dc=edt,dc=org"
>>>>>  AuthLDAPBindPassword    "secret"
>>>>>  #AuthLDAPURL "ldap://ldap.edt.org:389/dc=edt,dc=org?sAMAccountName?sub?(objectClass=*)" NONE
>>>>>  AuthLDAPURL "ldap://ldap.edt.org:389/dc=edt,dc=org?uid"
>>>>>  Require valid-user
>>>>> ```

>>> #### 2.1.3 WEB OAUTH

>>>> · Observem també que al accedir al web ens demana autenticació OAUTH.
>>
>>>> · Per accedir a aquesta web caldrà relitzar una autenticació Oauth corresponent a l’exercici de la Fase-1. És a dir, aquesta part és la realitzada en la Fase 1.

>>>> __· CREACIÓ CODI HTML__

>>>>> Crear el fitxer __html__ la part que és centra en el funcionament __intern__ i __relacional__ entre les webs.

>>>>> ```
>>>>> <!DOCTYPE html>
>>>>> <html>
>>>>>
>>>>>  <head>
>>>>>    <meta charset="utf-8">
>>>>>    <title>INICI</title>
>>>>>    <link rel="stylesheet" href="style.css">
>>>>>  </head>
>>>>>
>>>>>  <body>
>>>>>
>>>>>    <header>
>>>>>        <h1 class="tittle"> PROJECTE 2N TRIMESTRE </h1>
>>>>>        <center>
>>>>>            <a href="http://web.edt.org"><img src="images/logo.png" width="30%"></a>
>>>>>        </center>
>>>>>    </header>
>>>>>
>>>>>    <br>
>>>>>
>>>>>    <article>
>>>>>      <br>
>>>>>      <a href="http://webldap.edt.org/index.php" style="font-size: 35px; margin-right: 50px">LDAP</a>
>>>>>      <a href="https://webapirest.edt.org/index.html"style="font-size: 35px;">APIREST</a>
>>>>>      <br><br> 
>>>>>    </article>
>>>>>
>>>>>    <br>
>>>>>
>>>>>    <article>
>>>>>      <a href="https://localhost:8888" target="_blank"><u><h1 style="text-align: center;">SPOTIFY API</h1></u></a>
>>>>>      </p>
>>>>>      <iframe src="app.js" width="1050" height="700" name="iframe-a" style="background-color: white;"></iframe>
>>>>>      <br>  <br>
>>>>>    </article>
>>>>>
>>>>>  </body>
>>>>>  
>>>>>  <br>
>>>>>
>>>>>  <footer>
>>>>>    <u>
>>>>>    <br>
>>>>>    <strong> Autors: Maria Marlene Flor Benitez,  Kevin Gabriel Gonzalez Trujillo, Bruno Rodríguez Aranibar i Lucas Rodríguez Cabañeros </strong>
>>>>>    <br>
>>>>>    <strong>Gitlab: <a href="https://gitlab.com/a190074lr/projecte-trimestral/-/tree/main/2%20TRIMESTRE">https://gitlab.com/a190074lr/projecte-trimestral/-/tree/main/2%20TRIMESTRE</strong></a>
>>>>>    <br>
>>>>>    <strong>DockerHub: <a href="https://hub.docker.com/repositories/proyecto2hisx">https://hub.docker.com/repositories/proyecto2hisx</strong></a>
>>>>>    <br>
>>>>>    <strong> All rights deserved: 2HISX SL </strong>
>>>>>    <br><br>
>>>>>    </u>
>>>>>  </footer>
>>>>>
>>>>> </html>
>>>>> ```

>>>> __· FUNCIONAMENT APIREST__

>>>>> La idea inicial amb aquesta web era que en entrar a la pàgina ens demanès autenticació per part de tercers (Google) i en autenticar-nos ens redirigís directament a la aplicació web d'spotify. Però vam tenir problemes amb la redirecció i vam haver de fer una web a la qual sen's demana autenticació amb google i una vegada entrem tenim un enllaç que ens porta a la aplicació web. A la que també vam haver de fer canvis.
>
>>>>> El problema principal era que a l'hora de redirigir automàticament amb la configuració de OAuth aquesta no agafava localhost com i el seu corresponent port per obrir l'aplicació. La solució que vam trobar va ser redirigir a l'aplicació através d'un link una vegda ens haviem autenticat com usuaris de google. Aquí va aparéixer un nou problema que implicava la própia aplicació d'Spotify.
>
>>>>> El segon problema amb l'aplicació web era que l'aplicació s'hauria de definir amb una URI de redireccionament que per defecte era el localhost. Però donat que estem en un entorn en el núvol, a l'hora de redirigir cap al localhost desde el cloud, l'API no trovaba cap ruta cap a la qual enviar l'informació ja que per arribar a l'aplicació que estaba demanant dades aquest hauría de saber l'IP pública de la màquina on es troba. Per aquest motiu vam decidir utilitzar una IP elàstica a AWS per definir-la com a ruta en l'aplicació d'Spotify i d'aquesta manera cada vegada que llençem l'instància la IP es manté.
>
>>>>> Tenim definides dues vies de obtenció d'informació de l'API, amb dos comptes diferents. La de Bruno la tenim definida per defecte amb el localhost i la de Kevin la tenim definida amb la IP elàstica per AWS.
>
>>>>> __· BRUNO__
>
>>>>>  ![Bruno](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/2%20TRIMESTRE/Images/spotify_uris.png)
>
>>>>>  __· KEVIN__
>
>>>>>  ![Kevin](https://gitlab.com/a190074lr/projecte-trimestral/-/raw/main/2%20TRIMESTRE/Images/IP_elastica.png)
>
>>>>> Aquest problema amb l'IP pública només el tenim quan llençem una instància a AWS, no quan executem els contenidors en local. Per aquest motiu tenim definides les seguents líneas de codi en el startup del servidor web que permeten fer aquest canvi de credencials i redireccions.
>
>>>>> Aquestes lines de codi, només s'executen si estem en una instància AWS, el que fem és canviar el host al qual redirigeix el link que tenim definit a la pàgina i tambè canviem les credencials que s'utilitzen per desplegar l'aplicació, posem les de Kevin que té definida la IP elàstica en el seu compte.

>>>>>> ```
>>>>>> if [ $(facter ec2_metadata | wc -l) -gt 1 ]; then
>>>>>>	#SI es instancia AWS cambia codigos de la aplicacion i tambien cambia las redireciones de localhost a la IP publica de la instancia
>>>>>>	IP_PUBLICA=$(curl -s https://api.ipify.org)
>>>>>>	sed -i '/SPOTIFY API/s/localhost/'"$IP_PUBLICA"'/g' webapirest.edt.org/html/index.html
>>>>>>	sed -i '/redirect_uri/s/localhost/'"$IP_PUBLICA"'/g' webapirest.edt.org/html/app.js
>>>>>>
>>>>>>	CLIENT_ID=d43463be772f4956b8f215fc462b7944
>>>>>>	sed -i "s/client_id = '[^']*'/client_id = '$CLIENT_ID'/g"  webapirest.edt.org/html/app.js
>>>>>>
>>>>>>	CLIENT_SECRET=01985bf7bd24442db60e5dddfbd8caf0
>>>>>>	sed -i "s/client_secret = '[^']*'/client_secret = '$CLIENT_SECRET'/g" webapirest.edt.org/html/app.js
>>>>>> ```

>>  ## 2.2 SSL

>>> L'autenticació OAuth de la pàgina _webapirest.edt.org_ no era posible si no era per tràfic segur, per tant vam haver d'afegir certificats SSL a la pàgina.
>
>>> Primerament vam haver de crear un parell de clau/certificat autosignat per vincular-los a la we. Una vegada creats quan es llença el servidor web els coloquem al corresponent directori.
>
>>>> ```
>>>> # SSL/TLS certificate
>>>> mv webapirest.crt /etc/ssl/certs 
>>>> mv webapirest.key /etc/ssl/private
>>>> ```
>
>>> En fer aixó ens assegurem que els certificats estàn al seu lloc i podem establir les líneas de vinculació dels certificats al seu correspondent virtualhost.
>
>>>> ```
>>>> SSLEngine on
>>>> SSLCertificateFile /etc/ssl/certs/webapirest.crt
>>>> SSLCertificateKeyFile /etc/ssl/private/webapirest.key
>>>> ```

## 3. FASE 3: AWS VPC INFRASTRUCTURE

> Aquesta fase consisteix en implementar...
>
>> *· Desplegar a AWS un entorn de desenvolupament amb VPC usant varies zones com l’exemple descrit en el LAB de VPC.*
>
>> *· Desplegar usant terraform les tresus seus virtuals i tot el que requereixin generant una aplicació a AWS EC*

> ### 3.1 CREACIÓ DOCKER COMPOSE

>> Abans de crear-ne el fitxer de __Terraform__ que serà l'encarregat de llençar les instàncies a AWS, necessitem crear un docker compose que serà qui desplegui tots els dockers corresponents, en aquest cas:
>
>>> *· El docker __ldap__.*
>>
>>> *· El docker __postgres__.*
>>
>>> *· El docker __serverweb__ ( encarregat de desplegar el servei de les pàgines webs ).*
>
>> El document compose.yaml encen els tres contenidors diferents necesaries pel projecte en una __xarxa anomenada 2hisx__.

>> ```
>> version: "3.0"
>> services:
>>  ldap:
>>    image: proyecto2hisx/ldap:apache
>>    container_name: ldap.edt.org
>>    hostname: ldap.edt.org
>>    ports:
>>      - "389:389"
>>    networks:
>>      - 2hisx
>>  psql:
>>    image: proyecto2hisx/postgres:apache
>>    container_name: psql.edt.org
>>    hostname: psql.edt.org
>>    ports:
>>      - "5432:5432"
>>    environment:
>>      - POSTGRES_PASSWORD=passwd
>>      - POSTGRES_DB=training
>>    networks:
>>      - 2hisx
>>  serverweb:
>>    image: proyecto2hisx/serverweb:final
>>    container_name: serverweb.edt.org
>>    hostname: serverweb.edt.org
>>    ports:
>>      - "443:443"
>>      - "80:80"
>>      - "8888:8888"
>>    networks:
>>      - 2hisx
>>
>> networks:
>>   2hisx:
>> ```

>> La ordre necessaria per encendre el compose es: 

>> ```
>> docker compose up -d
>> ```

>> #### 3.1.1 LDAP

>>> El servei ldap es necesari per al pas de autenticacio mitjançant Ldap i dins del compose es defineixen el seguents parametres:
>>
>>>>  __· Imatge utilitzada:__ *S'utilitzara la imatge **proyecto2hisx/ldap:apache** del repositori **proyecto2hisx**.*
>>
>>>>  __· Nom del container:__ *ldap.edt.org.*
>>
>>>>  __· Nom del Host:__ *ldap.edt.org.*
>>
>>>>  __· Ports a propagar:__ *Port **389** del host local al port **389** del container.*
>>
>>>>  __· Xarxa:__ *El container estara dins la xarxa **2hisx**.*

>>> ```
>>>  ldap:
>>>    image: proyecto2hisx/ldap:apache
>>>    container_name: ldap.edt.org
>>>    hostname: ldap.edt.org
>>>    ports:
>>>      - "389:389"
>>>    networks:
>>>      - 2hisx
>>> ```

>> #### 3.1.2 PSQL

>>> El servei psql farà la funció de permetra fer consultas sql una vegada feta la autenticacio ldap en webldap.edt.org
>>
>>>> __· Imatge utilitzada:__ *S'utilitzara la imatge **proyecto2hisx/postgres:apache** del repositori **proyecto2hisx**.*
>>
>>>> __· Nom del container:__ *psql.edt.org.*
>>
>>>> __· Nom del Host:__ *psql.edt.org.*
>>
>>>> __· Ports a propagar:__ *Port **5432** del host local al port **5432** del container.*
>>
>>>> __· Variables d'entorn:__ *Definim la contraseña de l'usuari postgres i a quina BDD es connecta.*
>>
>>>>>```
>>>>> POSTGRES_PASSWORD=passwd
>>>>> POSTGRES_DB=training
>>>>>```
>>
>>>>__· Xarxa:__ *El container estara dins la xarxa **2hisx**.*

>>> ```
>>>  psql:
>>>    image: proyecto2hisx/postgres:apache
>>>    container_name: psql.edt.org
>>>    hostname: psql.edt.org
>>>    ports:
>>>      - "5432:5432"
>>>    environment:
>>>      - POSTGRES_PASSWORD=passwd
>>>      - POSTGRES_DB=training
>>>    networks:
>>>      - 2hisx
>>> ```

>> #### 3.1.3 SERVER WEB

>>> El servei __serverweb__ és l'encarregat de proveure el servidor web de apcahe que mostrara les pagines web.
>>
>>>> __· Imatge utilitzada:__ *S'utilitzara la imatge **proyecto2hisx/serverweb:final** del repositori **proyecto2hisx**.*
>>
>>>> __· Nom del container:__ *S'utilitzara la imatge **proyecto2hisx/serverweb:final** del repositori **proyecto2hisx**.*
>>
>>>> __· Nom del Host:__ *serverweb.edt.org.*
>>
>>>> __· Ports a propagar:__ *Port **5432** del host local al port **5432** del container.*
>>
>>>>>```
>>>>> Port 443 del host local al port 443 del container (https).
>>>>> Port 80 del host local al port 80 del container (http).
>>>>> Port 8888 del host local al port 8888 del container (app_spotify).
>>>>>```
>>
>>>>__· Xarxa:__ *El container estara dins la xarxa **2hisx**.*

>>> ```
>>>  serverweb:
>>>    image: proyecto2hisx/serverweb:final
>>>    container_name: serverweb.edt.org
>>>    hostname: serverweb.edt.org
>>>    ports:
>>>      - "443:443"
>>>      - "80:80"
>>>      - "8888:8888"
>>>    networks:
>>>      - 2hisx
>>> ```

>> #### 3.1.4 XARXA 2HISX

>>> Per ultim És crea la xarxa 2hisx per a que els containers puguin interactuar entre ells.
>>
>>> __· Creacio xarxa:__*És crea la xarxa **2hisx**.*

>>> ```
>>> networks:
>>>   2hisx:
>>> ```

> ### 3.1 DESPLEGAMENT DE INSTANCIA AWS AMB TERRAFORM 

>> El fitxer __main.tf__ crea una instancia en AWS mitjançant terraform que alhora s'aproveix de tot el necesari per a fer funcionar els serveis ldap , psql i serverweb.
>
>> Les comandes necesaries per a fer funcionar la instancia son:
>
>>> *· Preparar el sistema de terraform amb l'ordre:*
>
>>>> ```
>>>> terraform init
>>>> ```
>
>>> *· Executar les ordres del main.tf amb la comanda:.*
>
>>>> ```
>>>> terraform apply
>>>> ```
>
>>>>> *Nota:Aquestes ordres es tenen que executar al directori on hi ha definit el fitxer __main.tf__.
>
>>> *· Per ultim hi ha que modificar el **/etc/hosts** per poder accedir a les pagines web i posar la seguent linia.*
>
>>>> ```
>>>> 18.133.60.118 web.edt.org webldap.edt.org webapirest.edt.org
>>>> ```

>> #### 3.1.1 FITXER MAIN.TF

>>> ```
>>> terraform {
>>> required_providers {
>>>    aws = {
>>>      source  = "hashicorp/aws"
>>>      version = "~> 4.16"
>>>    }
>>> }
>>> }
>>>
>>> provider "aws" {
>>>  region     = "eu-west-2"
>>>  access_key = "AKIAQUYHMRZWXBWMF3NN"
>>>  secret_key = "Josg0ixgQxOuap+JJne/lXN90oVFi3cjLd/ZfKEK"
>>> }
>>>
>>> resource "aws_eip_association" "eip_assoc" {
>>>  instance_id   = "${aws_instance.example.id}"
>>>  allocation_id = "eipalloc-0180d32aeba990d61"
>>> }
>>>
>>> resource "aws_instance" "example" {
>>>  ami           = "ami-0d93d81bb4899d4cf" // replace with your desired AMI
>>>  instance_type = "t2.small" // replace with your desired instance type
>>>  key_name = "apache"
>>>  vpc_security_group_ids = [aws_security_group.webserver-sg.id]
>>>  tags = {
>>>    Name = "docker-instance"
>>>  }
>>> }
>>>
>>> resource "null_resource" "copy_script" {
>>>  depends_on = [aws_instance.example]
>>>
>>>  provisioner "file" {
>>>    source      = "./install_docker.sh"
>>>    destination = "/home/admin/install_docker.sh"
>>>
>>>    connection {
>>>      type        = "ssh"
>>>      user        = "admin"
>>>      private_key = file("apache.pem")
>>>      host        = "18.133.60.118"
>>>    }
>>>  }
>>> }
>>> resource "null_resource" "copy_yaml" {
>>>  depends_on = [null_resource.copy_script]
>>>
>>>  provisioner "file" {
>>>    source      = "./compose.yml"
>>>    destination = "/home/admin/compose.yml"
>>>
>>>    connection {
>>>      type        = "ssh"
>>>      user        = "admin"
>>>      private_key = file("apache.pem")
>>>      host        = "18.133.60.118"
>>>    }
>>>  }
>>> }
>>> resource "null_resource" "run_script" {
>>>  depends_on = [null_resource.copy_yaml]
>>>
>>>  provisioner "remote-exec" {
>>>    inline = [
>>>      "chmod +x /home/admin/install_docker.sh",
>>>      "bash /home/admin/install_docker.sh",
>>>      "sudo docker compose -f /home/admin/compose.yml up -d"
>>>    ]
>>>
>>>    connection {
>>>      type        = "ssh"
>>>      user        = "admin"
>>>      private_key = file("apache.pem")
>>>      host        = "18.133.60.118"
>>>    }
>>>  }
>>> }
>>>
>>> resource "aws_security_group" "webserver-sg" {
>>>  name = "webserver-sg"
>>>  description = "Puertos para funcionamiento del servidor web"
>>>
>>>  ingress {
>>>    description = "http"
>>>    from_port   = 80
>>>    to_port     = 80
>>>    protocol    = "tcp"
>>>    cidr_blocks = ["0.0.0.0/0"]
>>>  }
>>>
>>>  ingress {
>>>    description = "https"
>>>    from_port   = 443
>>>    to_port     = 443
>>>    protocol    = "tcp"
>>>    cidr_blocks = ["0.0.0.0/0"]
>>>  }
>>>
>>>  ingress {
>>>    description = "psql"
>>>    from_port   = 5432
>>>    to_port     = 5432
>>>    protocol    = "tcp"
>>>    cidr_blocks = ["0.0.0.0/0"]
>>>  }
>>>
>>>  ingress {
>>>    description = "spotify apirest"
>>>    from_port   = 8888
>>>    to_port     = 8888
>>>    protocol    = "tcp"
>>>    cidr_blocks = ["0.0.0.0/0"]
>>>  }
>>>
>>>  ingress {
>>>    description = "ldap"
>>>    from_port   = 389
>>>    to_port     = 389
>>>    protocol    = "tcp"
>>>    cidr_blocks = ["0.0.0.0/0"]
>>>  }
>>>
>>>  egress {
>>>    from_port   = 0
>>>    to_port     = 0
>>>    protocol    = "-1"
>>>    cidr_blocks = ["0.0.0.0/0"]
>>>  }
>>> }
>>>
>>> // Expose the EC2 instance's public IP address
>>> output "public_ip" {
>>>  value = "18.133.60.118"
>>> }
>>> ```

>> #### 3.1.2 PROVEIDORS 

>>> Primer hi ha que definir els proveidors que utilitzara terraform i aixi averiguar el proces que volem que faci.
>
>>> Terraform disposa de una llarga varietat de proveidors pero el que es necessari per a la practica es **aws**.La definicio del parametres que apareixen son:
>
>>>> __· required_providers:__ *Dins d'aquest segment definim els proveidors necesaris.*
>
>>>> __· aws:__ *Indiquem com a proveidor **aws** i dins especifiquem mes parametre.*
>
>>>> __· source:__ *Es d'on es aquest proveidor i on trobarlo.*
>
>>>> __· version:__ *Indiquem la versio que ens interesa.*
>
>>>>> ```
>>>>> terraform {
>>>>> required_providers {
>>>>>    aws = {
>>>>>      source  = "hashicorp/aws"
>>>>>      version = "~> 4.16"
>>>>>    }
>>>>> }
>>>>> }
>>>>> ```
>
>>> Despres s'indica parametres extras per a una configuracio mes complexa. En aquest s'indica el seguent:
>
>>>> __· region:__ *Es la regio on estar ubicada la nostra instacia.*
>
>>>> __· access_key i secret_key:__ *Son les calus necesaries per asociar la creacio de la intacia a un compte personal de amazon.*
>
>>>>> ```
>>>>> provider "aws" {
>>>>>  region     = "eu-west-2"
>>>>>  access_key = "AKIAQUYHMRZWWQQXPQ7P"
>>>>>  secret_key = "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"
>>>>> }
>>>>> ```

>>> #### 3.1.3 ASOCIACIO DE IP ELASTICA

>>> Per al bon funcionament del servei integrat de spotify fa falta asociar la instancia a una ip elastica de manera que la ip publica sempre sera la mateixa. Aixo es defineix en aquest apartat:
>
>>>> __· resource "aws_eip_association" "eip_assoc":__ *Definim un recurs  que especifica la asociacio de una ip publica a una intancia de AWS **(aws_eip_association)**.La identificacio d'aquest recurs sera __(eip_assoc)__.*
>
>>>> __· instance_id:__ *Es l'identificador de la instancia sobre la cual s'associa la ip elastica.Aquesta s'aconsegueix amb la variable **${aws_instance.example.id}** que s'assigna cuan la instancia de aws es creada.*
>
>>>> __· allocation_id:__ *Es l'identificador de la ip elastica que utilitzarem i que associarem.
(Per aconseguir la id fa falta crear la ip elastica amb antelacio en la web d'administracio de intancies a Amazon).*
>
>>>>> ```
>>>>> resource "aws_eip_association" "eip_assoc" {
>>>>>  instance_id   = "${aws_instance.example.id}"
>>>>>  allocation_id = "eipalloc-0180d32aeba990d61"
>>>>> }
>>>>> ```
 
>> #### 3.1.4 CREACIO INSTANCIA AWS

>>> En aques tpas es crea la instancia de AWS definint caracteristiques importants en la seva creacio com la ami a utilitzar i el tipus d'instancia.Aquests es difineixen d'aquesta manera:
>
>>>> __· resource "aws_instance" "example":__ *Es crea un recurs que crea la intancia aws __(aws_instance)__ i te com a identificador __(example)__.*
>
>>>> __· ami:__ *S'especifica la id de la ami que volem utilitzar.En aquest cas
__(ami-0d93d81bb4899d4cf)__ seria utilitzar la ami de __(Debian11)__.*
>
>>>> __· instance_type:__ *Diem quina capacitat de procesament volem.Normalment seria una __(t2.micro)__ pero com els serveis exigeixen una alta capacitat de processament aqui s'utilitza una __(t2.small)__.*
>
>>>> __· key_name:__ *Es la clau .pem amb la que ens permet conectar a la instancia mitjançant ssh.*
>
>>>> __· vpc_security_group_ids:__ *Aqui es defineix amb que security group s'asociara aquesta instancia i per tant que ports es propagaran.Aquesta esta definida amb la variable __([aws_security_group.webserver-sg.id])__.*
>
>>>> __· tags:__ *Son els tags amb les que s'identificar la intancia.*
>
>>>>>    __· Name:__ *Es el nom que tindra la instacia dins de web d'administracio de intancies a Amazon.*
>
>>>>> ```
>>>>> resource "aws_instance" "example" {
>>>>>  ami           = "ami-0d93d81bb4899d4cf" // replace with your desired AMI
>>>>>  instance_type = "t2.small" // replace with your desired instance type
>>>>>  key_name = "apache"
>>>>>  vpc_security_group_ids = [aws_security_group.webserver-sg.id]
>>>>>  tags = {
>>>>>    Name = "docker-instance"
>>>>>  }
>>>>> }
>>>>> ```

>> #### 3.1.5 COPIAR FITXERS LOCALS A LA INSTANCIA

>>> Per a poder fer funcionar els serveis definits en el compose necesitem copiar el recurs __install_docker.sh__ i __compose.yml__ dins la intancia.El primer arxiu es un script que instala totes les dependencies necesaries per a fer funcionar docker en la instancia i el segon arxiu es un __.yml__ que conte els serveis a llançar.
>
>>> Per a fer la accio esmentada hi ha que definir els seguents parametres dos vegades (un per a copiar install_docker i la'altra per a copiar el compose.yml):
>
>>>> __· resource "null_resource" "copy_script":__ *Es un recurs que no pertany a un proveidor en concret __(null_resource)__ i te com a identificador __(copy_script)__ per a copiar __install_docker__ o __(copy_yaml)__ per a copiar el __compose.yml__.*
>
>>>> __· depends_on = [aws_instance.example]:__ *Es una condicio d'execucio del recurs el cual no s'executara si el proces definit  ha fallat.En aquest cas seria  __([aws_instance.example])__ fa referencia al proces de creacio de la instancia AWS o __([null_resource.copy_script])__ que es l'altre recurs per a copiar un fitxer.*
>
>>>> __· provisioner "file":__ *Es el proces per a proveir a la instancia amb un fitxer.*
>
>>>>>    __· source:__ *S'indica la ruta del fitxer local que volem copiar.*
>
>>>>>    __· destination:__ *S'especifica la ruta desti on es copia el fitxer.*
>
>>>> __· key_name:__ *Crea una conexio a la instancia.*
>
>>>> __· vpc_security_group_ids:__ *Aqui es defineix amb que security group s'asociara aquesta instancia i per tant que ports es propagaran.Aquesta esta definida amb la variable __([aws_security_group.webserver-sg.id])__.*
>
>>>>>    __· type:__ *Diem que tipus de conexio volem.En aquest cas __(ssh)__.*
>
>>>>>    __· user:__ *Es l'usuari amb qui ens conectarem.En aquest cas __(admin)__.*
>
>>>>>    __· private_key:__ *Definim amb que clau .pem ens verifiquem.En aquest cas __(apache.pem)__.*
>
>>>>>    __· host:__ *Comuniquem quina sera la ip del host desti.Aqui hi ha que posar la ip elastica abans creada __(18.133.60.118)__.*
>
>>>>> ```
>>>>> resource "null_resource" "copy_script" {
>>>>>  depends_on = [aws_instance.example]
>>>>>
>>>>>  provisioner "file" {
>>>>>    source      = "./install_docker.sh"
>>>>>    destination = "/home/admin/install_docker.sh"
>>>>>
>>>>>    connection {
>>>>>      type        = "ssh"
>>>>>      user        = "admin"
>>>>>      private_key = file("apache.pem")
>>>>>      host        = "18.133.60.118"
>>>>>    }
>>>>>  }
>>>>> }
>>>>>
>>>>> resource "null_resource" "copy_yaml" {
>>>>>  depends_on = [null_resource.copy_script]
>>>>>
>>>>>  provisioner "file" {
>>>>>    source      = "./compose.yml"
>>>>>    destination = "/home/admin/compose.yml"
>>>>>
>>>>>    connection {
>>>>>      type        = "ssh"
>>>>>      user        = "admin"
>>>>>      private_key = file("apache.pem")
>>>>>      host        = "18.133.60.118"
>>>>>    }
>>>>>  }
>>>>> }
>>>>> ```

>> #### 3.1.6 DESPLEGAMENT DELS SERVEIS

>>> Per a desplegar els serveis ho farem de forma que nosaltres proveim les ordres bash per executar el fitxer d'instalacio de docker i posar en marxa el compose.yml amb els serveis.
>
>>> El recurs necesari es defineix de la seguent forma:
>
>>>> __· resource "null_resource" "run_script":__ * Es un recurs que no pertany a un proveidor en concret __(null_resource)__ i te com a identificador __(run_script)__ .*
>
>>>> __· depends_on = [null_resource.copy_yaml]:__ *Es una condicio d'execucio del recurs el cual no s'executara si el proces definit  ha fallat.En aquest cas seria  __([null_resource.copy_yaml])__.*
>
>>>> __· provisioner "remote-exec":__ *Utilitzem un provisioner que executa ordres en la instancia de forma remota __(remote-exec)__.*
>
>>>>>    __· inline:__ *Es proporciona les ordres bash a executar que son els seguents:*
>
>>>>>>       __· chmod +x /home/admin/install_docker.sh:__ *Cambia els permisos del fitxer __(install_docker.sh)__ per a ser un executable.*
>
>>>>>>       __· bash /home/admin/install_docker.sh:__ *Posem en marxa els serveis en background amb docker compose i el fitxer __(compose.yml)__.*
>
>>>>>>       __· sudo docker compose -f /home/admin/compose.yml up -d:__ *Cambia els permisos del fitxer __(install_docker.sh)*__ per a ser un executable.*
>
>>>> __· connection:__ *Crea una conexio a la instancia.*
>
>>>> __· vpc_security_group_ids:__ *Aqui es defineix amb que security group s'asociara aquesta instancia i per tant que ports es propagaran.Aquesta esta definida amb la variable __([aws_security_group.webserver-sg.id])__.*
>
>>>>>    __· type:__ *Diem que tipus de conexio volem.En aquest cas __(ssh)__.*
>
>>>>>    __· user:__ *Es l'usuari amb qui ens conectarem.En aquest cas __(admin)__.*
>
>>>>>    __· private_key:__ *Definim amb que clau .pem ens verifiquem.En aquest cas __(apache.pem)__.*
>
>>>>>    __· host:__ *Comuniquem quina sera la ip del host desti. Aqui hi ha que posar la ip elastica abans creada __(18.133.60.118)__.*
>
>>>>> ```
>>>>> resource "null_resource" "run_script" {
>>>>>  depends_on = [null_resource.copy_yaml]
>>>>>
>>>>>  provisioner "remote-exec" {
>>>>>    inline = [
>>>>>      "chmod +x /home/admin/install_docker.sh",
>>>>>      "bash /home/admin/install_docker.sh",
>>>>>      "sudo docker compose -f /home/admin/compose.yml up -d"
>>>>>    ]
>>>>>
>>>>>    connection {
>>>>>      type        = "ssh"
>>>>>      user        = "admin"
>>>>>      private_key = file("apache.pem")
>>>>>      host        = "18.133.60.118"
>>>>>    }
>>>>>  }
>>>>> }
>>>>> ```

>> #### 3.1.7 CONFIGURAR SECURITY GROUPS

>>>  En aquest apartat definim els ports que s'han d'obrir en el security groups vinculat a aquesta instància. Els ports necessaris són: 80 (http), 443 (https), 389 (ldap), 5432 (psql) i el 8888 (aplicació spotify). 
>
>>> Els ports a obrir es defineixen de la seguent manera:
>
>>>> __· resource "aws_security_group" "webserver-sg":__ *Defineix un recurs per l'especificació de ports a obrir en el security group.*
>
>>>> __· name:__ *Defineix el nom que tindrà el security group creat.*
>
>>>> __· description:__ *Opcional per descriure la finalitat del grup de ports.*
>
>>>> __· ingress:__ *Amb aquesta directiva indiquem el port a obrir.*
>
>>>>>    __· descriptiob:__ *Descripció del port (opcional).*
>
>>>>>    __· from_port:__ *Numero de port inicial que volem obrir.*
>
>>>>>    __· to_port:__ *Numero de port inicial que volem obrir.*
>
>>>>>    __· protocol:__ *Aqui especifiquem si els ports han de ser tipus TCP o UDP.*
>
>>>>>    __· cidr_blocks:__ *Especifiquem quin rang d'adreçes o quines adreçes específiques permetem que entrin. En aquest cas utilitzem un wildcard que significa totes les ipv4.*
>
>>>> __· egress:__ *En aquesta part es defineix el tràfic de sortida que té les mateixes directives que les d'entrada. Però per defecte AMS permet tot el tràfic de sortida i simplement ho reflectim així amb aquests valors..*
>
>>>>> ```
>>>>> resource "aws_security_group" "webserver-sg" {
>>>>>  name = "webserver-sg"
>>>>>  description = "Puertos para funcionamiento del servidor web"
>>>>>
>>>>>  ingress {
>>>>>    description = "http"
>>>>>    from_port   = 80
>>>>>    to_port     = 80
>>>>>    protocol    = "tcp"
>>>>>    cidr_blocks = ["0.0.0.0/0"]
>>>>>  }
>>>>>
>>>>>  ingress {
>>>>>    description = "https"
>>>>>    from_port   = 443
>>>>>    to_port     = 443
>>>>>    protocol    = "tcp"
>>>>>    cidr_blocks = ["0.0.0.0/0"]
>>>>>  }
>>>>>
>>>>>  ingress {
>>>>>    description = "psql"
>>>>>    from_port   = 5432
>>>>>    to_port     = 5432
>>>>>    protocol    = "tcp"
>>>>>    cidr_blocks = ["0.0.0.0/0"]
>>>>>  }
>>>>>
>>>>>  ingress {
>>>>>    description = "spotify apirest"
>>>>>    from_port   = 8888
>>>>>    to_port     = 8888
>>>>>    protocol    = "tcp"
>>>>>    cidr_blocks = ["0.0.0.0/0"]
>>>>>  }
>>>>>
>>>>>  ingress {
>>>>>    description = "ldap"
>>>>>    from_port   = 389
>>>>>    to_port     = 389
>>>>>    protocol    = "tcp"
>>>>>    cidr_blocks = ["0.0.0.0/0"]
>>>>>  }
>>>>>
>>>>>  egress {
>>>>>    from_port   = 0
>>>>>    to_port     = 0
>>>>>    protocol    = "-1"
>>>>>    cidr_blocks = ["0.0.0.0/0"]
>>>>>  }
>>>>> }
>>>>> ```

>> #### 3.1.8 EXPOSAR LA CLAU PÚBLICA

>>>  Per ultim pasem per pantalla la ip publica de la instancia per a facilitar una conexio ssh.
>
>>> Aquest pas es fa d'aquesta forma:
>
>>>> __· output "public_ip":__ * S'especifica que es mostrara el valor de una variable anomenada __(public_ip)__.*
>
>>>>>    __· value = "18.133.60.118":__ *El valor d'aquesta variable sera la ip elastica asociada a la instancia.*
>
>>>>> ```
>>>>> // Expose the EC2 instance's public IP address
>>>>> output "public_ip" {
>>>>>  value = "18.133.60.118"
>>>>> }
>>>>> ```
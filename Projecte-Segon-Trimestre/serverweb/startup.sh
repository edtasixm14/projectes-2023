#! /bin/bash

# comprovar si se ejecuta en maquina local o instancia AWS
if [ $(facter ec2_metadata | wc -l) -gt 1 ]; then
	#SI es instancia AWS cambia codigos de la aplicacion i tambien cambia las redireciones de localhost a la IP publica de la instancia
	IP_PUBLICA=$(curl -s https://api.ipify.org)
	sed -i '/SPOTIFY API/s/localhost/'"$IP_PUBLICA"'/g' webapirest.edt.org/html/index.html
	sed -i '/redirect_uri/s/localhost/'"$IP_PUBLICA"'/g' webapirest.edt.org/html/app.js

	CLIENT_ID=d43463be772f4956b8f215fc462b7944
	sed -i "s/client_id = '[^']*'/client_id = '$CLIENT_ID'/g"  webapirest.edt.org/html/app.js

	CLIENT_SECRET=01985bf7bd24442db60e5dddfbd8caf0
	sed -i "s/client_secret = '[^']*'/client_secret = '$CLIENT_SECRET'/g" webapirest.edt.org/html/app.js


fi

# root dir de los virtualhost 
cp -r web.edt.org /var/www/
cp -r webldap.edt.org /var/www/
cp -r webapirest.edt.org /var/www/

# creamos los virtualhost 
cp virtualhost/* /etc/apache2/sites-available/

# declaramos los virtualhost creando symbolic links de sites-available a sites-enabled
ln -s /etc/apache2/sites-available/web.edt.conf /etc/apache2/sites-enabled/
ln -s /etc/apache2/sites-available/webldap.edt.conf /etc/apache2/sites-enabled/
ln -s /etc/apache2/sites-available/webapirest.edt.conf /etc/apache2/sites-enabled/

# config php
cp php/php.ini /etc/php/7.4/apache2
# SSL/TLS certificate
mv webapirest.crt /etc/ssl/certs 
mv webapirest.key /etc/ssl/private

# modulo ssl 
a2enmod ssl 
# modulo proxy 
a2enmod proxy
a2enmod proxy_http

# modulo de autorización y autenticación LDAP en apache2
a2enmod authnz_ldap

# levantamos applicacion spotify 
node /var/www/webapirest.edt.org/html/app.js & 

# activamos el servicio 
service apache2 start

# keep container running
tail -f /dev/null 


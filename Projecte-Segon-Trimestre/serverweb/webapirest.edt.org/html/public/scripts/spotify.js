// Recoje los valores que recibe la api por http

(function() {

  /**
   * Obtains parameters from the hash of the URL
   * @return Object
   */
  function getHashParams() {
    var hashParams = {};
    var e, r = /([^&;=]+)=?([^&;]*)/g,
        q = window.location.hash.substring(1);
    while ( e = r.exec(q)) {
       hashParams[e[1]] = decodeURIComponent(e[2]);
    }
    return hashParams;
  }

  var userProfileSource = document.getElementById('user-profile-template').innerHTML,
      userProfileTemplate = Handlebars.compile(userProfileSource),
      userProfilePlaceholder = document.getElementById('user-profile');

  var oauthSource = document.getElementById('oauth-template').innerHTML,
      oauthTemplate = Handlebars.compile(oauthSource),
      oauthPlaceholder = document.getElementById('oauth');

  var params = getHashParams();

// Valores que recoje la primera funcion ** 
  var access_token = params.access_token,
      refresh_token = params.refresh_token,
      error = params.error;
// Comprubea si el error de auth existe
  if (error) {
    alert('There was an error during the authentication');
// Si todo funciona bien
  } else {
    // Comprueba si existe acces_token 
    if (access_token) {
      // render oauth info
      oauthPlaceholder.innerHTML = oauthTemplate({
        access_token: access_token,
        refresh_token: refresh_token
      });
      // Si existe la peticion ajax, obteniendo los datos del endpoint
      $.ajax({
          url: 'https://api.spotify.com/v1/me',
          headers: {
            'Authorization': 'Bearer ' + access_token
          },
          success: function(response) {
            userProfilePlaceholder.innerHTML = userProfileTemplate(response);

            $('#login').hide();
            $('#loggedin').show();
          }
      });
    } else {
        // render initial screen
        $('#login').show();
        $('#loggedin').hide();
    }

    // Refresca el token lanzando una nueva petición ajax (No se implementa)
    document.getElementById('obtain-new-token').addEventListener('click', function() {
      $.ajax({
        url: '/refresh_token',
        data: {
          'refresh_token': refresh_token
        }
      }).done(function(data) {
        access_token = data.access_token;
        oauthPlaceholder.innerHTML = oauthTemplate({
          access_token: access_token,
          refresh_token: refresh_token
        });
      });
    }, false);

    //boton clear results, funcion jQuer empty 
    $("#clear_results").click(function(){
      $('#artists').empty();
      $('#top_tracks').empty();  
    });

    // NUEVA PETICION AJAX 
    document.getElementById('button').addEventListener('click',function(){
      
      // limpiamos resultados anteriores al realizar un nueva busqueda
      $('#artists').empty();
      $('#top_tracks').empty();

      console.log($('#name').val());
      // guardamos el valor ingresado por el usuario, nombre del artista
      var artistName = $('#name').val();

      // comprobamos si hay espacios en el nombre del artista 
      if ($('#name').val().indexOf(" ") != -1){
        var replaceSpace = $.trim($('#name').val());
        artistName = replaceSpace.replace(/ /g, "%20");
      }
      // Primera peticion, recoje el id del artista ** 
      if (access_token) {
        $.ajax({
          // utilizando el parámetro "q" para indicar el nombre del artista y el parámetro "type" para especificar que se está buscando un "artista" en particular.
          url: 'https://api.spotify.com/v1/search?q=' + artistName + '&type=artist',
       
          market: 'ES',
       
          headers: {
       
              'Authorization': 'Bearer ' + access_token
          },
          //Cuando la petición se completa correctamente y se recibe una respuesta de la API de Spotify.
          success: function(response) {
              console.log(access_token);
              console.log(response);
              // muestra por consola los artistas que contiene el nom 
              console.log(response.artists.items[0].name);

              // almacenamos la lista de artistas que se ha obtenido 
              var artistsArray = response.artists.items;
              console.log(artistsArray);
              
              var $id;
              //recorreremos el array "artists" uno por uno, utilizando metodo $each de jQuery para iterar sobre cada elemento del array
              $.each(artistsArray, function(key,value){
                // almacenamos el nombre del artista
                var $name = artistsArray[key].name;
                console.log($name);
                // almacenamos el ID del artista
                $id = artistsArray[key].id;
                console.log($id);

                var $nameDiv = $("<div class='name'/>");
                // crea un nuevo elemento HTML button con el ID del artista actual, que se almacena en la variable "$button".
                var $button = $("<button class='button' id =" + $id + "/>");
                var $object = $("<div class='object'/>");

                $nameDiv.append($name);
                $button.append($nameDiv);
                $object.append($button);
                $("#artists").append($object);
                // Como resultado crea una lista de botones de artistas en la página web a partir de la respuesta de la API
              });

              // Segunda peticion relacionada con la primera petición (una peticion detro de otra peticion) **
              $('.button').click(function(){

                console.log(this.id);
                // la llamda se ejecutara cuando el usuario seleccion cualquier artista de la lista resultante mostrando el top tracks
                $.ajax({
                  url: 'https://api.spotify.com/v1/artists/' + this.id + '/top-tracks?country=ES',
                  type: "GET",
                  headers:{
                    'Authorization': 'Bearer ' + access_token,
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                  },
                  // Cuando la petición se completa correctamente, recibe un objeto con las canciones mas populares del artista
                  success:function(result){
                    console.log(result);

                    // bucle para recorrer las canciones del artista pantalla/consola
                    $.each(result.tracks, function(key,value){
                      
                      console.log(value.name + " " + value.uri);
                      var $top_track = value.name;
                      var $link = $('<a/>').addClass("link").attr("href",value.uri);
                      // Construimos un nuevo elemento button para cada canción
                      var $new_button = $("<button class='new_button'/>");
                      var $new_object = $("<div class='new_object'/>");

                      $new_button.append($top_track);
                      $link.append($new_button);
                      $new_object.append($link);
                      $("#top_tracks").append($new_object);

                    });
                  },
                  error:function(error){
                    console.log(error)
                  }
                });

              });
            }
       });
      } else{
        alert('There was an error during the authentication');
      }
    });
  }
})();

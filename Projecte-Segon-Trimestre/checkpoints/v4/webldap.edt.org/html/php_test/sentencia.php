<!DOCTYPE html>
<html>
<head>
	<title>Consulta SQL</title>
</head>
<body>
	<h1>Consulta SQL</h1>
	<form method="POST" action="">
		<label for="sql">Ingrese una consulta SQL:</label>
		<textarea id="sql" name="sql" rows="5" cols="50"></textarea>
		<br>
		<input type="submit" value="Consultar">
	</form>

    <?php
        // Conexión a la base de datos training
        $dbconn = pg_connect("host=psql.edt.org dbname=training user=postgres password=passwd") or die('No se pudo conectar: ' . pg_last_error());
        
        // si se ingresa un valor en el formulario 
        if (isset($_POST["sql"])) {
            // Sentencia SQL para extraer los datos que deseamos mostrar
            $query = $_POST["sql"];
            $result = pg_query($dbconn, $query) or die('La consulta falló: ' . pg_last_error());

            // Mostrar los resultados en una tabla HTML
            echo "<table>";
            echo "<tr>";
            // Creamos dinámicamente los encabezados segun el número de campos
            for ($i = 0; $i < pg_num_fields($result); $i++) {
                echo "<th>" . pg_field_name($result, $i) . "</th>";
            }
            echo "</tr>";

            // pg_fetch_assoc() retorna una matriz asociativa a los datos obtenidos con la sentencia SQL
            while ($row = pg_fetch_assoc($result)) {
                echo "<tr>";
                // Por cada fila, mostramos datos 
                foreach ($row as $field) {
                    echo "<td>" . $field . "</td>";
                }
                echo "</tr>";
            }
            echo "</table>";
        }
        // Cerrar la conexión a la base de datos
        pg_close($dbconn);
    ?>
</body>
</html>
# Projectes 2023

## Escola del Treball de Barcelona

### ASIX Administració de sistemes informàtics en xarxa

#### Curs 2022-2023

#### M14 Projecte

ASIX M14 Projectes Escola del treball de Barcelona. Curs 2022-2023.


**Projecte segon / tercer trimestre**

  * Descripció: [Projecte_primer_trimestre.pdf](https://gitlab.com/edtasixm14/projectes-2023/-/blob/main/M14-Projecte-primer-trimestre.pdf)
  
    GITLAB: https://gitlab.com/a190074lr/projecte-trimestral

    DOCKERHUB: https://hub.docker.com/repository/docker/marleneflor/projecte1t/general

**Projectes pla antic**

  * Kubernetes [Projecte Kubernetes jordi Quiros](https://github.com/jordiiqb/kubernetes)
  * FreeIPA [Projecte FreeIPA Juan Sánchez](https://gitlab.com/isx49328692/projecto_final/-/tree/main)
  * FreeIPA [projecte FreeIPA Alejandro Gambin](https://gitlab.com/Algamg/freeipa-m14)

